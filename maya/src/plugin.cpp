

#include <maya/MStatus.h>
#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>
#include "errorMacros.h"
#include "attributeFu.h"

MStatus initializePlugin(MObject obj)
{
	MStatus st;
	MString method("initializePlugin");
	MFnPlugin plugin(obj, PLUGIN_VENDOR, PLUGIN_VERSION, MAYA_VERSION);
	
		// BUILD UI
	MGlobal::executePythonCommandOnIdle("from cry import menu;menu.CryMenu()", true);

	st = plugin.registerCommand("attributeFu", attributeFu::creator, attributeFu::newSyntax);
	msert;
	return st;
}

MStatus uninitializePlugin(MObject obj)
{
	MStatus st;
	MString method("uninitializePlugin");
	MFnPlugin plugin(obj);
	st = plugin.deregisterCommand("attributeFu");
	return st;
}
