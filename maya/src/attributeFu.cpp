#include <maya/MPlugArray.h>
#include <maya/MTypeId.h>
#include <maya/MFnAttribute.h>
#include <maya/MPxNode.h>

#include "attributeFu.h"


void* attributeFu::creator()
{
	return new attributeFu();
}

MSyntax attributeFu::newSyntax()
{
	MSyntax syn;
	
	syn.addFlag(kAACFlag, kAACFlagL);
	syn.addFlag(kStaticFlag, kStaticFlagL);

	syn.setObjectType(MSyntax::kStringObjects, 0, 1);
	syn.useSelectionAsDefault(true);
	
	return syn;
		
}


MString attributeFu::getColonDelimited(MFnAttribute & attrFn) {
	MStatus st;
	MString info = "";
	MString	aaCmd = attrFn.getAddAttrCmd(false);
	MStringArray fields;
	st = aaCmd.split(' ' , fields);
	MString name, shortName, type ;
	MString defaultVal = "n/a";
	for (unsigned j=0;j<(fields.length() - 1);j++) {
		const MString & f = fields[j];
		const MString & ff = fields[(j+1)];
		MStringArray tmpArray;
		if (f == "-ln") {
			ff.split('"',tmpArray);
			name = tmpArray[0];
		} else if (f == "-sn") {
			ff.split('"',tmpArray);
			shortName = tmpArray[0];
		} else if ((f == "-dt") ||(f == "-at") ) {
			ff.split('"',tmpArray);
			type = tmpArray[0];
		} else if ((f == "-dv") ) {
			ff.split('"',tmpArray);
			defaultVal  = tmpArray[0];
		} 
	}
	info+=":name=";
	info+=name;
	info+=":shortName=";
	info+=shortName;
	info+=":type=";
	info+=type;
	info+=":default=";
	info+=defaultVal;
	
	int depth;

	st = MS::kSuccess;
	depth = -1;
	//MFnAttribute pAttrFn(att);
	
	
//	bool readable, writable, connectable, storable, array, keyable, hidden;
	info+=":readable="; 
	info+=	attrFn.isReadable();
	info+=":writable="; 
	info+=	attrFn.isWritable();
	info+=":connectable="; 
	info+=attrFn.isConnectable();
	info+=":storable="; 
	info+=	attrFn.isStorable();
	info+=":array="; 
	info+=	attrFn.isArray();
	info+=":keyable="; 
	info+=	attrFn.isKeyable();
	info+=":hidden="; 
	info+= 	attrFn.isHidden();
	
	
	do  {
		depth++;
		MObject p = attrFn.parent(&st);
		if (! st.error()) attrFn.setObject(p);
	} while (! st.error());

	
	
	info+=":depth="; 
	info+= depth;	

	info+= "\n";

	return info;
}


MStatus attributeFu::doIt( const MArgList& args )
{
	MStatus s;
//	unsigned int counter = 0;
	MString method("attributeFu::doIt");
	
	MArgDatabase  argData(syntax(), args);


	MStringArray argStrings;
	
	argData.getObjects(argStrings);
	// cerr << argStrings << endl;
	if (argStrings.length() > 1) return MS::kUnknownParameter;
	
	MString node = argStrings[0];
	MString plug("");

	MStringArray split;
	node.split('.' , split );
	node = split[0];
	if (split.length() > 1)
		plug = split[(split.length() - 1)];
	
	MSelectionList list;
	list.add(node);
	// cerr << node << endl;

	MItSelectionList nodeIter(list);
	if (nodeIter.isDone()) {
		displayError("Must pick at least one node");
		return MS::kUnknownParameter;
	}
	
	MObject  nodeObject;
	nodeIter.getDependNode( nodeObject );
	MFnDependencyNode nodeFn( nodeObject );

	int numAtts =  nodeFn.attributeCount();
	//MFn::Type  type = nodeFn.type();
	//bool onePlug = (plug != "");
	
	
	if (plug != "") {
		MObject att = nodeFn.attribute(plug, &s);

		if (! s.error()) {
			MFnAttribute attrFn(att);
			if (argData.isFlagSet(kAACFlag)) {
				appendToResult(attrFn.getAddAttrCmd(false));
			} else {
				appendToResult(getColonDelimited(attrFn));
			}
			return MS::kSuccess;
		}
		
		return MS::kUnknownParameter;
	}

	for (int i = 0;i<numAtts;i++){
		
		MString info("");
		MObject att = nodeFn.attribute(i);
		MFnAttribute attrFn(att);
		if ((argData.isFlagSet(kStaticFlag)) && (attrFn.isDynamic())) continue;
		if (argData.isFlagSet(kAACFlag)) {
			appendToResult(attrFn.getAddAttrCmd(false));
		} else {
			appendToResult(getColonDelimited(attrFn));
		}
	}	
	
	return MS::kSuccess;
}





