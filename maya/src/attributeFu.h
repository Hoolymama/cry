#ifndef	__attributeFu_H__
#define	__attributeFu_H__

#include <maya/MIOStream.h>
#include <math.h>

#include <maya/MPxCommand.h>
#include <maya/MItSelectionList.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnAttribute.h>
#include <maya/MFnDependencyNode.h>



#define kAACFlag			"-a"
#define kAACFlagL 			"-addAttrCmd"

#define kStaticFlag			"-s"
#define kStaticFlagL 		"-static"

class attributeFu : public MPxCommand

{
	public:
	
	attributeFu() {}
	virtual ~attributeFu() {}
	
	MStatus doIt( const MArgList& args );
	
	static void* creator();
	
   static MSyntax      newSyntax();	

	private:
	MString getColonDelimited(MFnAttribute & attrFn) ;

};

#endif

