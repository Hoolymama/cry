import pymel.core as pm

from cry import remove_multi_tab, connector_tab, permutations_tab

import importlib


def reload():
    print("Reloading Cry")
    importlib.reload(remove_multi_tab)
    importlib.reload(connector_tab)
    importlib.reload(permutations_tab)
    

    pm.mel.source("scribe", language="mel")
    pm.mel.source("scribe_node", language="mel")
    pm.mel.source("scribe_command", language="mel")
