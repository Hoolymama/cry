import pymel.core as pm
import pymel.core.uitypes as gui
import re

class RemoveMultiTab(gui.FormLayout):
    def __init__(self):
        self.setNumberOfDivisions(100)
        pm.setParent(self)


        self.att_field = pm.textField(text="--")

        self.create_action_buttons_and_layout()

        self.att_popup = pm.popupMenu(
            parent=self.att_field,
            pmc=pm.Callback(self.post_menu_command),
            )

    def create_action_buttons_and_layout(self):
        pm.setParent(self)  # form

        dry_but = pm.button(label="Dry run", command=pm.Callback(self.on_dry))
        go_but = pm.button(label="Remove", command=pm.Callback(self.on_go))

        self.attachForm(self.att_field, "left", 2)
        self.attachForm(self.att_field, "right", 2)
        self.attachForm(self.att_field, "top", 2)
        self.attachNone(self.att_field, "bottom")

        # /////////////
        self.attachNone(dry_but, "top")
        self.attachForm(dry_but, "left", 2)
        self.attachPosition(dry_but, "right", 2, 50)
        self.attachForm(dry_but, "bottom", 2)

        self.attachNone(go_but, "top")
        self.attachForm(go_but, "right", 2)
        self.attachPosition(go_but, "left", 2, 50)
        self.attachForm(go_but, "bottom", 2)
        # /////////////
 

    def post_menu_command(self):
        print("post_menu_command")
        pm.popupMenu(self.att_popup, edit=True, deleteAllItems=True)
        pm.setParent(self.att_popup, menu=True)
        try:
            nodes = pm.ls(sl=True)
        except IndexError:
            pm.menuItem(label="Nothing selected")
            return
        node =  nodes[0]
        print("node", node)
        atts = self.get_attribute_templates(node)
        print("atts", atts)
        for att in atts:               
            pm.menuItem(label=att,  command=pm.Callback(self.set_attr_template, att))

    def set_attr_template(self, att):
        pm.textField(self.att_field, edit=True, text=att)


    def on_go(self, dryrun=False):
        nodes = pm.ls(sl=True) 
        unconnected_only = True
        attr = pm.textField(self.att_field, query=True, text=True)
        for node in nodes:
            # node = pm.PyNode(node)
            try:
                plugs = list(pm.PyNode(node).attr(attr))
            except:
                print("Node: {} has no array attribute: {}".format(node, attr))
                continue
            count = 0

            for plug in plugs:
                if unconnected_only and plug.isConnected():
                    continue
                count+=1
                if not dryrun:
                    pm.removeMultiInstance(plug, b=True)
            print("Removed {} plugs from {}".format(count, node))


    def on_dry(self):
        self.on_go(dryrun=True)


    @staticmethod
    def get_attribute_templates(node):
        result = []
        attrs = pm.listAttr(node)
        for attr in attrs:
            try:
                attname = node.attr(attr).name()
                if node.attr(attr).isArray():
                    result.append(attname.partition(".")[2])

            except (RuntimeError, pm.MayaAttributeError):
                pass
        
        return sorted(list(set(result)))
