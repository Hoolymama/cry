import pymel.core.uitypes as gui
import pymel.core as pm

import importlib
from cry import reloader
from cry import remove_multi_tab, connector_tab, permutations_tab

importlib.reload(reloader)

class AttrUtilsWindow(gui.Window):
    def __init__(self):

        others = pm.lsUI(windows=True)
        for win in others:
            if pm.window(win, q=True, title=True) == "Attribute Utilities":
                pm.deleteUI(win)

        self.setTitle("Attribute Utilities")
        self.setIconName("Attribute Utilities")
        self.setWidthHeight([700, 600])

        self.menuBarLayout = pm.menuBarLayout()

        self.tabs = pm.tabLayout(
            changeCommand=pm.Callback(self.on_tab_changed))

        pm.setParent(self.tabs)
        self.remove_multi_tab = remove_multi_tab.RemoveMultiTab()
        self.tabs.setTabLabel((self.remove_multi_tab, "Remove Multis"))

        pm.setParent(self.tabs)
        self.connector_tab = connector_tab.ConnectorTab()
        self.tabs.setTabLabel((self.connector_tab, "Connectify"))

        pm.setParent(self.tabs)
        self.permutations_tab = permutations_tab.PermutationsTab()
        self.tabs.setTabLabel((self.permutations_tab, "Permutations"))

        self.show()
        self.setResizeToFitChildren()
        self.populate()

    def on_tab_changed(self):
        self.save()

    def populate(self):
        var = ("cry_att_tab_index", 1)
        self.tabs.setSelectTabIndex(pm.optionVar.get(var[0], var[1]))

    def save(self):
        var = "cry_att_tab_index"
        pm.optionVar[var] = self.tabs.getSelectTabIndex()



