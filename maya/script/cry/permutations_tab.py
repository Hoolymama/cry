import pymel.core as pm
import pymel.core.uitypes as gui
import itertools


class PermutationsTab(gui.FormLayout):
    def __init__(self):
        self.setNumberOfDivisions(100)
        pm.setParent(self)

        self.add_rows_but = pm.button(
            label="Add Node/Attr Rows", command=pm.Callback(self.add_rows)
        )

        self.start_frame_field_grp = pm.intFieldGrp(label="Start Frame", numberOfFields=1, value1=1)
        pm.setParent(self)

        self.header_row_form = self.create_header_row()
        pm.setParent(self)

        self.column = pm.columnLayout(adj=True)

        self.create_action_buttons_and_layout()

    def create_header_row(self):
        pm.setParent(self)
        row_form = pm.formLayout(nd=100)
        node_attr_field = pm.text(label="Node/Attr", align="center", bgc=(0.4, 0.4, 0.4), height=24)
        start_val_field = pm.text(
            label="Start value", align="center", bgc=(0.4, 0.4, 0.4), height=24
        )
        end_val_field = pm.text(label="End value", align="center", bgc=(0.4, 0.4, 0.4), height=24)
        num_vals_field = pm.text(label="Num values", align="center", bgc=(0.4, 0.4, 0.4), height=24)
        exponent_field = pm.text(label="Exponent", align="center", bgc=(0.4, 0.4, 0.4), height=24)
        delete_but = pm.text(label="", align="center", bgc=(0.4, 0.4, 0.4), height=24)

        self.layoutRow(
            row_form,
            node_attr_field,
            start_val_field,
            end_val_field,
            num_vals_field,
            exponent_field,
            delete_but,
        )

        return row_form

    def create_action_buttons_and_layout(self):
        pm.setParent(self)  # form

        dry_but = pm.button(label="Dry run", command=pm.Callback(self.on_dry))
        go_but = pm.button(label="Set Keys", command=pm.Callback(self.on_go))

        self.attachForm(self.add_rows_but, "left", 2)
        self.attachNone(self.add_rows_but, "right")
        self.attachForm(self.add_rows_but, "top", 2)
        self.attachNone(self.add_rows_but, "bottom")

        self.attachNone(self.start_frame_field_grp, "left")
        self.attachForm(self.start_frame_field_grp, "right", 2)
        self.attachForm(self.start_frame_field_grp, "top", 2)
        self.attachNone(self.start_frame_field_grp, "bottom")

        self.attachForm(self.header_row_form, "left", 2)
        self.attachForm(self.header_row_form, "right", 2)
        self.attachControl(self.header_row_form, "top", 2, self.add_rows_but)
        self.attachNone(self.header_row_form, "bottom")

        self.attachNone(dry_but, "top")
        self.attachForm(dry_but, "left", 2)
        self.attachPosition(dry_but, "right", 2, 50)
        self.attachForm(dry_but, "bottom", 2)

        self.attachNone(go_but, "top")
        self.attachForm(go_but, "right", 2)
        self.attachPosition(go_but, "left", 2, 50)
        self.attachForm(go_but, "bottom", 2)

        self.attachForm(self.column, "left", 2)
        self.attachForm(self.column, "right", 2)
        self.attachControl(self.column, "top", 2, self.header_row_form)
        self.attachControl(self.column, "bottom", 2, go_but)

    def selected_node_attrs(self):

        obs = pm.channelBox("mainChannelBox", q=True, mainObjectList=True) or []
        atts = pm.channelBox("mainChannelBox", q=True, selectedMainAttributes=True) or []
        result = []
        for att in atts:
            for ob in obs:
                result.append(ob + "." + att)

        obs = pm.channelBox("mainChannelBox", q=True, historyObjectList=True) or []
        atts = pm.channelBox("mainChannelBox", q=True, selectedHistoryAttributes=True) or []
        for att in atts:
            for ob in obs:
                result.append(ob + "." + att)
        return result

    def add_rows(self):
        node_attrs = self.selected_node_attrs()
        for node_attr in node_attrs:
            self.add_row(node_attr)

    def add_row(self, node_attr):

        pm.setParent(self.column)
        row_form = pm.formLayout(nd=100)

        node_attr_field = pm.textField()
        node_attr_field.setText(node_attr)
        start_val_field = pm.floatField()
        start_val_field.setValue(0.0)
        end_val_field = pm.floatField()
        end_val_field.setValue(1.0)
        num_vals_field = pm.intField()
        num_vals_field.setValue(3)
        exponent_field = pm.floatField()
        exponent_field.setValue(1.0)
        delete_but = pm.symbolButton(
            image="smallTrash.xpm", c=pm.Callback(self.delete_row, row_form)
        )

        pm.setParent(self.column)
        self.layoutRow(
            row_form,
            node_attr_field,
            start_val_field,
            end_val_field,
            num_vals_field,
            exponent_field,
            delete_but,
        )

    def layoutRow(
        self,
        row_form,
        node_attr_field,
        start_val_field,
        end_val_field,
        num_vals_field,
        exponent_field,
        delete_but,
    ):

        row_form.attachForm(node_attr_field, "left", 2)
        row_form.attachPosition(node_attr_field, "right", 2, 30)
        row_form.attachForm(node_attr_field, "top", 2)
        row_form.attachForm(node_attr_field, "bottom", 2)

        row_form.attachControl(start_val_field, "left", 2, node_attr_field)
        row_form.attachPosition(start_val_field, "right", 2, 45)
        row_form.attachForm(start_val_field, "top", 2)
        row_form.attachForm(start_val_field, "bottom", 2)

        row_form.attachControl(end_val_field, "left", 2, start_val_field)
        row_form.attachPosition(end_val_field, "right", 2, 60)
        row_form.attachForm(end_val_field, "top", 2)
        row_form.attachForm(end_val_field, "bottom", 2)

        row_form.attachControl(num_vals_field, "left", 2, end_val_field)
        row_form.attachPosition(num_vals_field, "right", 2, 75)
        row_form.attachForm(num_vals_field, "top", 2)
        row_form.attachForm(num_vals_field, "bottom", 2)

        row_form.attachControl(exponent_field, "left", 2, num_vals_field)
        row_form.attachPosition(exponent_field, "right", 2, 90)
        row_form.attachForm(exponent_field, "top", 2)
        row_form.attachForm(exponent_field, "bottom", 2)

        row_form.attachControl(delete_but, "left", 2, exponent_field)
        row_form.attachForm(delete_but, "right", 2)
        row_form.attachForm(delete_but, "top", 2)
        row_form.attachForm(delete_but, "bottom", 2)

    def delete_row(self, row_form):
        pm.deleteUI(row_form)

    def _clear_entries(self, which):
        col = self.src_column if which == "src" else self.dest_column
        children = pm.columnLayout(col, q=True, ca=True)
        if children:
            pm.deleteUI(children)


    def on_go(self, dry=False):
        rows = pm.columnLayout(self.column, q=True, ca=True)
        frame = pm.intFieldGrp(self.start_frame_field_grp , q=True, value1=True)
        source_vals = []
        permutations = []
        node_attrs = []
        for row in rows:
            row_children = pm.formLayout(row, q=True, ca=True)

            num_vals = pm.intField(row_children[3], q=True, value=True)
            exponent = pm.floatField(row_children[4], q=True, value=True)
            start_val = pm.floatField(row_children[1], q=True, value=True)
            end_val = pm.floatField(row_children[2], q=True, value=True)

            attr_vals = []
            for i in range(num_vals):
                fraction = i / (num_vals - 1)
 
                attr_vals.append(start_val + ((end_val - start_val) * (fraction**exponent)))
            source_vals.append(attr_vals.copy())
            node_attrs.append(pm.textField(row_children[0], q=True, text=True))

        permutations = list(itertools.product(*source_vals))

        
        for i in range(len(permutations)):
            frame_number = frame + i
            frame_set = zip(node_attrs, permutations[i])
            for pair in frame_set:
                node = pm.PyNode(pair[0]).node()
                attr = pm.Attribute(pair[0]).attrName()
                if dry:
                    print("{} . {} >> {}".format(node,attr, pair[1]))
                else:
                    print("Setting key for {} . {} >> {}".format(node,attr, pair[1]))
                    pm.setKeyframe(node, at=attr, t=frame_number, v=pair[1])
           
            print("---------------------------------")


    def on_dry(self):
        self.on_go(dry=True)
