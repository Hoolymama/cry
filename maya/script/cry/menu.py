import pymel.core as pm

import os
import glob

# from  cry  import window
# from  cry.rich_save  import RichSaveWindow
import importlib
from cry import attr_utils, reloader
import importlib


MAYA_PARENT_WINDOW = "MayaWindow"
HOOLY_MENU = "HoolyMenu"


def ensure_hooly_menu():
    menu = next((m for m in pm.lsUI(menus=True) if m.getLabel() == "Hooly"), None)
    if menu:
        return menu
    return pm.menu(HOOLY_MENU, label="Hooly", tearOff=True, parent=MAYA_PARENT_WINDOW)


class CryMenu(object):
    def __init__(self):
        pm.mel.source("matchRename", language="mel")
        pm.mel.source("ezRename", language="mel")
        pm.mel.source("autosave", language="mel")
        pm.mel.source("scribe", language="mel")
        pm.mel.source("scribe_node", language="mel")
        pm.mel.source("scribe_command", language="mel")

        self.hooly_menu = ensure_hooly_menu()
        pm.setParent(self.hooly_menu, menu=True)
        pm.menuItem(divider=True, dividerLabel="Cry")
        pm.menuItem(label="Match Rename", command=pm.Callback(match_rename))
        pm.menuItem(label="EZ Rename", command=pm.Callback(ez_rename))

        # AUTOSAVE
        pm.menuItem(label="Autosave", subMenu=True)
        pm.menuItem(label="Start", command=pm.Callback(autosave_setup, 1))
        pm.menuItem(optionBox=True, command=pm.Callback(autosave_ui))
        pm.menuItem(label="Stop in Session", command=pm.Callback(autosave_kill_session))
        pm.menuItem(label="Stop for ever", command=pm.Callback(autosave_kill_forever))
        pm.setParent("..", menu=True)

        pm.menuItem(label="Attribute Utils", command=pm.Callback(open_attr_utils_win))
 
        # DOC
        pm.menuItem(label="Scribe", subMenu=True)
        pm.menuItem(label="Document Node", command=pm.Callback(pm.mel.scribe_doc_node))
        pm.menuItem(label="Document Command", command=pm.Callback(pm.mel.scribe_doc_command))
        pm.menuItem(label="Document Script", command=pm.Callback(pm.mel.scribe_doc_script))

        pm.setParent("..", menu=True)

        pm.menuItem(label="Reload Cry", command=pm.Callback(reloader.reload))
        pm.mel.autosave_setup(0)


def match_rename():
    pm.mel.matchRename()


def ez_rename():
    pm.mel.ezRename()


def autosave_setup(force):
    pm.mel.autosave_setup(force)


def autosave_ui():
    pm.mel.autosave_win()


def autosave_kill_session():
    pm.mel.autosave_kill(0)


def autosave_kill_forever():
    pm.mel.autosave_kill(1)


def open_attr_utils_win():
    importlib.reload(attr_utils)
    importlib.reload(reloader)
    reloader.reload()
    win = attr_utils.AttrUtilsWindow()
