
import pymel.core as pm
import sanity.core.check as sc
import sanity.core.util as su


label_tip = """Meshes with open borders cause problems for boolean
operations, physics, and may produce unwanted render artifacts."""

check_tip = """Check for open borders.
We use polyListComponentConversion and polySelectConstraint
to find open borders. Any open borders will be added to the
hilight list for this check, in which case the Fix button
will be enabled."""

fix_tip = """Fix selected open borders with the polyCloseBorder command."""

hilight_tip = "Hilight edges and put tool into edge-select mode."

reset_tip = "Reset original selection and put tool into object-select mode."



class Klass(sc.SanityCheck):
    
    def __init__(self, **kwargs):
        super(Klass, self).__init__(**kwargs)

    def check(self, objects=None, **kwargs):

        objects = su.get_meshes(objects)
        if not objects:
            self.append_to_errors('No mesh objects provided')
            return False
        sel=pm.ls(selection=True)
        will_fix = False        
        offence_count = 0
        self.start_progress(1)
        edges = pm.polyListComponentConversion(objects, toEdge=True)
        pm.select(edges)

        if pm.about(version=True).split()[0] == '2014':
            pm.polySelectConstraint(bo=True, mode=2, crease=False)
            edges = pm.ls(sl=True)
            pm.polySelectConstraint(bo=False, mode=0, crease=False)
        else:
            pm.polySelectConstraint(bo=True, mode=2)
            edges = pm.ls(sl=True)
            pm.polySelectConstraint(bo=False, mode=0)      

        if edges:
            self.append_to_hilight_list(edges)
            for edge in edges:
                self.append_to_warnings('Border Edge: ' + edge.name(), verbose=2) 
            offence_count = len(pm.ls(edges, flatten=True))
            self.append_to_warnings(str(offence_count) + ' border edges found ')
            will_fix = True
        else:
            self.append_to_infos('No border edges found')
        pm.select(sel)
        return will_fix

    def fix(self, objects=None, **kwargs):
        grouped_comps = su.group_components_by_object(objects)
        if not grouped_comps:
            self.append_to_errors('Nothing to fix')
            return
        object_count = 0   
        for key in grouped_comps:
            pm.polyCloseBorder(grouped_comps[key])
            object_count += 1
            self.append_to_infos('Closed border edges on: ' + key, 2)
        self.append_to_infos('Closed border edges for: ' + str(object_count) + ' objects')
        
    def after_select_hilight_list(self):
        pm.selectMode(component=True)
        pm.selectType(edge=True)

    def after_reset_button_click(self):
        pm.selectMode(object=True)

    def label(self):
        return 'Open Borders'

    def author(self):
        return {'name': 'Julian Mann', 'email': 'julian.mann@gmail.com'}
