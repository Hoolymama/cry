__all__ = [
    "open_borders",
    "floating_vertices",
    "zero_length_edges",
    "non_quads",
    "non_manifold",
    "lamina_faces",
    "vertex_valence",
    "infacing_normals"
]

LABEL = 'Geometry'
