import random
import pymel.core as pm
import sanity.core.check as sc
import sanity.core.util as su

label_tip = """Mesh normals should face outwards.

Inwards facing normals may be the result of
scaling objects by -1. They can cause render artifacts
and interfere with potntial render optimizations."""

check_tip = """Check for objects whose normals face inwards.

Non-manifold geometry or objects with open borders have
no sense of inside or outside. Therefore, objects must
pass the open borders check and the non-manifold check
before this check can run.

This check works as follows:
It fires a ray from a small distance along the normal
of a triangle towards the object and counts the intersections.
An even number of hits means the normal faces out. An odd number
means if faces in and needs to be fixed. To avoid potential
accuracy errors, 6 intersection tests are performed from random
triangles. If most of them have an even number of hits , then \
the whole check passes."""

fix_tip = """Fix inward facing normals with the polyNormal command.
This fix operates on selected meshes."""

class Klass(sc.SanityCheck):

    def __init__(self, **kwargs):
        super(Klass, self).__init__(**kwargs)

    def check(self, objects=None, **kwargs):
        objects = su.get_meshes(objects)
        if not objects:
            self.append_to_errors('No mesh objects provided')
            return False

        if self.__selection_is_non_manifold(objects):
            self.append_to_errors(
                'At least one selected object is non-manifold. Please fix non-manifold objects before running this test')
            return False
        if self.__selection_has_open_borders(objects):
            self.append_to_errors(
                'At least one selected object has open borders. Please fix open bordered objects before running this test')
            return False

        # by the time we get here we should have closed objects
        # with all normals facing either in or out - not both.
        will_fix = False
        object_count = 0
        offence_count = 0
        num_objects = len(objects)
        self.start_progress(num_objects)
        for mesh in objects:
            if not self.__normals_point_out(mesh):
                self.append_to_hilight_list(mesh)
                offence_count += 1
                self.append_to_warnings(mesh.name() + ': normals face inside.', 2)
            object_count += 1
            self.update_progress(object_count)
        if offence_count:
            self.append_to_warnings(
                    str(offence_count) + ' objects have in-facing normals.')
            will_fix = True
        else:
            self.append_to_infos('All normals face outwards')
        return will_fix

    def fix(self, objects=None, **kwargs):
        objects = su.get_meshes(objects)
        if not objects:
            self.append_to_errors('No mesh objects provided')
            return
        pm.polyNormal(objects, normalMode=0, userNormalMode=1, ch=0)

    # for each of half a dozen triangles:
    # Fire a ray from a small distance along normals
    # towards the object and count intersections.
    # An even number of hits means the normal faces out
    # If most normals face out, return true
    def __normals_point_out(self, mesh):
        num_samples = 6
        nf = mesh.numFaces()
        if nf < num_samples:
            num_samples = nf
        ids = random.sample(range(nf), num_samples)
        results = []
        verts = mesh.getPoints()
        for i in ids:
            vert_ids = mesh.getPolygonTriangleVertices(i, 0)
            v0 = verts[vert_ids[0]]
            v1 = verts[vert_ids[1]]
            v2 = verts[vert_ids[2]]
            pt = (v0 + v1 + v2) / 3.0
            normal = ((pm.datatypes.Vector(
                v1 - v0)).cross(pm.datatypes.Vector(v2 - v0))).normal()
            ray_origin = pt + (normal * 0.0001)
            ray_direction = pt - ray_origin
            hits = mesh.intersect(ray_origin, ray_direction)
            is_even = hits[0]
                # zero is not considered even, it is considered a miss
            if is_even:
                num_hits = len(hits[1])
                # if odd
                if num_hits % 2 != 0:
                    is_even = False
            results.append(is_even)

        if results.count(True) > results.count(False):
            return True
        return False

    def __selection_is_non_manifold(self, objects):
        # polyCleanupArgList works on selection only AFAIK, so we
        # make a note of the selection, select the given objects, 
        # then restore the selection
        sel = pm.ls(sl=True)
        pm.select(objects)
        args = ["0", "2", "0", "0", "0", "0", "0", "0", "0",
                "1e-05", "0", "1e-05", "0", "1e-05", "0", "1", "0"]
        non_manifold = pm.mel.polyCleanupArgList(3, args)
        pm.select(sel)
        if non_manifold:
            return True
        return False

    def __selection_has_open_borders(self, objects):
        # again - following commands need selection, so we
        # make a note of the selection, select the given objects, 
        # then restore the selection
        sel = pm.ls(sl=True)
        pm.select(objects)
        edges = pm.polyListComponentConversion(sel, toEdge=True)
        pm.select(edges)
        pm.polySelectConstraint(bo=True, mode=2)
        edges = pm.ls(sl=True)
        pm.polySelectConstraint(bo=False, mode=0)
        pm.select(sel)
        if edges:
            return True
        return False

    def label(self):
        return 'Infacing Normals'

    def author(self):
        return {'name': 'Julian Mann', 'email': 'julian.mann@gmail.com'}

