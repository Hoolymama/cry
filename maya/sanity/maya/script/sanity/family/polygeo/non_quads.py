import pymel.core as pm
import sanity.core.check as sc
import sanity.core.util as su

label_tip = """Quads are easier to texture, rig, and subdivide."""

check_tip = """Check for polygons that are not quads.

If found, they will be added to the hilight_list the fix 
button will be enabled."""

fix_tip = """Fix non-quad polygons using the polyQuad command.

This command may not resolve the problem completely.
You may have to fix some polygons by hand."""

hilight_tip = "Hilight non-quad faces and put tool into face-select mode."

reset_tip = "Reset original selection and put tool into object-select mode."


class Klass(sc.SanityCheck):

    def __init__(self, **kwargs):
        super(Klass, self).__init__(**kwargs)

    def check(self, objects=None, **kwargs):
        objects = su.get_meshes(objects)
        if not objects:
            self.append_to_errors('No mesh objects provided')
            return False
        will_fix = False
        object_count = 0
        offence_count = 0
        num_objects = len(objects)
        self.start_progress(num_objects)
        for mesh in objects:
            for f in mesh.f:
                nv = f.polygonVertexCount()
                if nv != 4:
                    self.append_to_hilight_list(f)
                    self.append_to_warnings(
                        'Face ' + f.name() + ' is ' + str(nv) + ' sided', 2)
                    offence_count += 1
            object_count += 1
            self.update_progress(object_count)
        if offence_count:
            self.append_to_warnings(str(offence_count) + ' polygons are not quads')
            will_fix = True
        else:
            self.append_to_infos('All polygons are quads')
        return will_fix

    def fix(self, objects=None, **kwargs):
        if not objects:
            self.append_to_errors('No objects provided')
            return
        pm.polyQuad(objects, ch=False, a=180.0)

    def after_select_hilight_list(self):
        pm.selectMode(component=True)
        pm.selectType(facet=True)

    def after_reset_button_click(self):
        pm.selectMode(object=True)

    def label(self):
        return 'Non Quads'

    def author(self):
        return {'name': 'Julian Mann', 'email': 'julian.mann@gmail.com'}
