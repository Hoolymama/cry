import pymel.core as pm
import sanity.core.check as sc
import sanity.core.util as su


label_tip = "Zero length edges produce unwanted subdivision issues."

check_tip = "Check for zero length edges."

fix_tip = """Fix zero length edges on selected objects.
NOTE: When the check fails, vertices of the offending edges are selected
so you can see where the problems are. These vertices are not considered
a valid selection for the fix. Therefore, associated objects will be fixed."""

class Klass(sc.SanityCheck):
    """Zero length edges produce unwanted results when subdividing.
    """

    def __init__(self, **kwargs):
        super(Klass, self).__init__(**kwargs)

    def check(self, objects=None, **kwargs):
        objects = su.get_meshes(objects)
        if not objects:
            self.append_to_errors('No mesh objects provided')
            return False

        will_fix = False
        sel = pm.ls(selection=True)
        pm.select(objects)
        self.start_progress(1)
        args = ["0", "2", "1", "0", "0", "0", "0", "0",
                "0", "1e-05", "1", "1e-05", "0", "1e-05", "0", "-1", "0"]
        zero_length = pm.mel.polyCleanupArgList(3, args)
        if zero_length:
            for z in zero_length:
                self.append_to_warnings('Zero length edge: ' + z, 2)

            offence_count = len(pm.ls(zero_length, flatten=True))
            self.append_to_warnings(str(offence_count) + ' zero length edges')
            # add verts to the hilight list because selected zero length edges
            # dont show up
            verts = pm.polyListComponentConversion(
                zero_length, toVertex=True)
            self.append_to_hilight_list(verts)

            will_fix = True
        else:
            self.append_to_infos('No Zero length edges found')
        pm.select(sel)
        return will_fix

    def fix(self, objects=None, **kwargs):
        objects = su.get_meshes(objects)
        if not objects:
            self.append_to_errors('No mesh objects provided')
            return
        pm.select(objects)
        args = ["0", "1", "1", "0", "0", "0", "0", "0", "0",
                "1e-05", "1", "1e-05", "0", "1e-05", "0", "-1", "0"]
        zero_length = pm.mel.polyCleanupArgList(3, args)
        self.append_to_infos('Zero length edges fixed.')

    def label(self):
        return 'Zero Length Edges'

    def author(self):
        return {'name': 'Julian Mann', 'email': 'julian.mann@gmail.com'}
