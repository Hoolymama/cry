import pymel.core as pm
import sanity.core.check as sc
import sanity.core.util as su


label_tip =  """Vertex Valence is defined as the number of edges 
that meet at a vertex. Vertex valence higher than eleven is unacceptable."""

check_tip = """ Check for vertices with eleven or more connected edges.
Any offending vertices will be added to the hilight_list."""

fix_tip = """Delete selected edges."""

hilight_tip = "Hilight edges and put tool into edge-select mode."

reset_tip = "Reset original selection and put tool into object-select mode."



class Klass(sc.SanityCheck):

    def __init__(self, **kwargs):
        super(Klass, self).__init__(**kwargs)

    def check(self, objects=None, **kwargs):

        objects = su.get_meshes(objects)
        if not objects:
            self.append_to_errors('No mesh objects provided')
            return False
        will_fix = False        
        object_count = 0
        offence_count = 0
        num_objects = 0
        for mesh in objects:
            num_objects += mesh.numVertices()
        self.start_progress(num_objects)
        for mesh in objects:
            for v in mesh.vtx:
                edges = list(v.connectedEdges())
                ne = len(edges)
                if ne > 10:
                    offence_count += 1
                    for edge in edges:
                        self.append_to_hilight_list(edge)
                    self.append_to_warnings('Vertex Valence '+str(ne)+' : ' +v.name(),2)
                object_count += 1
                self.update_progress(object_count)

        if offence_count:
            self.append_to_warnings(
                        str(offence_count) + ' vertices Vertex have valence above 11')
            will_fix = True
        else:
            self.append_to_infos('No excessive vertex valence')
        return will_fix

    def fix(self, objects=None, **kwargs):
        verts = pm.filterExpand(objects,sm=32)
        if verts:
            pm.delete(verts)

    def after_select_hilight_list(self):
        pm.selectMode(component=True)
        pm.selectType(edge=True)

    def after_reset_button_click(self):
        pm.selectMode(object=True)

    def label(self):
        return 'Valence above 10'

    def author(self):
        return {'name': 'Julian Mann', 'email': 'julian.mann@gmail.com'}
