import pymel.core as pm
import sanity.core.check as sc
import sanity.core.util as su

label_tip = """A floating vertex is pointless.
It has exactly 2 parallel connected edges.
Floating vertices produce bad subdivision because what may look
like a quad for example, is actually a 5 sided polygon."""

check_tip = """ Check for vertices with exactly two connected parallel edges.
Any offending vertices will be added to the hilight_list."""

fix_tip = "Delete selected vertices."

hilight_tip = "Hilight vertices and put tool into vertex-select mode."

reset_tip = "Reset original selection and put tool into object-select mode."


class Klass(sc.SanityCheck):

    def __init__(self, **kwargs):
        super(Klass, self).__init__(**kwargs)

    def check(self, objects=None, **kwargs):
        objects = su.get_meshes(objects)
        if not objects:
            self.append_to_errors('No mesh objects provided')
            return False

        will_fix = False
        object_count = 0
        offence_count = 0
        num_objects = 0
        for mesh in objects:
            num_objects += mesh.numVertices()

        self.start_progress(num_objects)
        for mesh in objects:
            for v in mesh.vtx:
                edges = list(v.connectedEdges())
                if len(edges) == 2:
                    e1 = edges.pop()
                    e0 = edges.pop()
                    vec0 = pm.datatypes.Vector(
                        e0.getPoint(1) - e0.getPoint(0))
                    vec1 = pm.datatypes.Vector(
                        e1.getPoint(1) - e1.getPoint(0))
                    if vec0.isParallel(vec1, tol=0.0001):
                        self.append_to_hilight_list(v)
                        self.append_to_warnings('Floating vertex: ' + v.name(), verbose=2)
                        offence_count += 1
                object_count += 1
                self.update_progress(object_count)
        if offence_count:
            self.append_to_warnings(str(offence_count) + ' floating vertices found')
            will_fix = True
        else:
            self.append_to_infos('No floating vertices')
        return will_fix


    def fix(self, objects=None, **kwargs):
        if objects:
            objects = pm.filterExpand(objects, sm=31)
            if objects:
                pm.delete(objects)

    def after_select_hilight_list(self):
        pm.selectMode(component=True)
        pm.selectType(vertex=True)

    def after_reset_button_click(self):
        pm.selectMode(object=True)

    def label(self):
        return 'Floating Vertices'

    def author(self):
        return {'name': 'Julian Mann', 'email': 'julian.mann@gmail.com'}
