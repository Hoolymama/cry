import pymel.core as pm
import sanity.core.check as sc
import sanity.core.util as su


label_tip = """Non-manifold geometry causes many problems.
Maya will refuse to subdivide it, booleans will fail, 
smooth operations will fail or give bad results and so on.

There are three different types of nonmanifold Geometry
(actually four since lamina faces are technically also
nonmanifold):

* Three or more faces share the same edge on an object.
* Two or more faces share the same vertex, yet they share no edge.
* Two or more adjacent faces have opposite normal directions."""

check_tip = """We make use of polyCleanupArgList to find non-manifold geometry.
All non-manifold edges and vertices will be found."""

fix_tip = """Fix non-manifold geometry on selected objects.
NOTE: When the check fails, offending components are selected so 
you can see where the problems are. These components are not considered
a valid selection for the fix. Therefore, associated objects will be fixed."""

class Klass(sc.SanityCheck):

    def __init__(self, **kwargs):
        super(Klass, self).__init__(**kwargs)

    def check(self, objects=None, **kwargs):
        objects = su.get_meshes(objects)
        if not objects:
            self.append_to_errors('No mesh objects provided')
            return False

        will_fix = False
        sel = pm.ls(selection=True)
        pm.select(objects)
        self.start_progress(1)
        args = ["0", "2", "0", "0", "0", "0", "0", "0",
                "0", "1e-05", "0", "1e-05", "0", "1e-05", "0", "1", "0"]
        non_manifold = pm.mel.polyCleanupArgList(3, args)
        if non_manifold:
            for m in non_manifold:
                self.append_to_warnings('Non manifold component: ' + m, 2)
            offence_count = len(pm.ls(non_manifold, flatten=True))
            self.append_to_warnings(str(offence_count) + ' non-manifold components')
            self.append_to_hilight_list(non_manifold)
            will_fix = True
        else:
            self.append_to_infos('No non-manifold geometry found')
        pm.select(sel)
        return will_fix

    def fix(self, objects=None, **kwargs):
        objects = su.get_meshes(objects)
        if not objects:
            self.append_to_errors('No mesh objects provided')
            return
        pm.select(objects)
        args = ["0", "1", "0", "0", "0", "0", "0", "0", "0",
                "1e-05", "0", "1e-05", "0", "1e-05", "0", "1", "0"]
        non_manifold = pm.mel.polyCleanupArgList(3, args)
        offence_count = len(pm.ls(non_manifold, flatten=True))
        self.append_to_infos('Non-manifold components cleaned.')
        self.append_to_warnings(
                'Check the cleaned objects as split vertices may need to be rejoined')

    def label(self):
        return 'Non Manifold Faces'

    def author(self):
        return {'name': 'Julian Mann', 'email': 'julian.mann@gmail.com'}
