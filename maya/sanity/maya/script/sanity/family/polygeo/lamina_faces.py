import pymel.core as pm
import sanity.core.check as sc
import sanity.core.util as su

label_tip = """Meshes with lamina faces are bad.

A lamina face is one that shares all vertices with another face.
Lamina faces are hard to spot by eye and they give unwanted
results when subdivided. Fortunately they are easy to clean up."""

check_tip =  """Check for lamina faces.

We use polyInfo to find lamina faces.
Any non-quad polygons will be added to the hilight_list
for this check, in which case the Fix button will be enabled.
"""

fix_tip = """Fix lamina faces by removing them.

If the check fails, the fix button will be enabled and will
delete selected faces.
"""


class Klass(sc.SanityCheck):

    def __init__(self, **kwargs):
        super(Klass, self).__init__(**kwargs)

    def check(self, objects=None, **kwargs):
        objects = su.get_meshes(objects)
        if not objects:
            self.append_to_errors('No mesh objects provided')
            return False

        will_fix = False

        offence_count = 0
        self.start_progress(1)
        faces = pm.polyInfo(objects, laminaFaces=True)
        if faces:
            offence_count += len(faces)
            self.append_to_hilight_list(faces)
            self.append_to_warnings(str(offence_count) + ' lamina faces found')
            for f in faces:
                self.append_to_warnings('Lamina face ' + f, 2)
            will_fix = True
        else:
            self.append_to_infos('No lamina faces found')
        return will_fix

    def fix(self, objects=None, **kwargs):
        objects = pm.filterExpand(objects, sm=34)
        if objects:
            pm.delete(objects)

    def label(self):
        return 'Lamina Faces'

    def author(self):
        return {'name': 'Julian Mann', 'email': 'julian.mann@gmail.com'}
