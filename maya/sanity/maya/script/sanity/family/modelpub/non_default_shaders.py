import pymel.core as pm
import sanity.core.check as sc
import sanity.core.util as su

label_tip = """Objects should be assigned the default lambert shader before publishing.

By assigning a default lambert shader before publishing,
downstream artists can see immediately what the model is composed of.
Frustrations caused by transparent shaders, and unassigned objects
are avoided, and artists don't have to spend time reassigning."""

check_tip = "Check objects that are not assigned the default lambert shader."

fix_tip = "Assign the default lambert shader to all selected objects."

class Klass(sc.SanityCheck):

    def __init__(self, **kwargs):
        super(Klass, self).__init__(**kwargs)

    def check(self, objects=None, **kwargs):
        objects = su.get_geometry(objects)
        if not objects:
            self.append_to_errors('Nothing selected')
            return False

        will_fix = False
        ig = pm.PyNode('initialShadingGroup')
        object_count = 0
        offence_count = 0
        num_objects = len(objects)
        self.start_progress(num_objects)
        for obj in objects:
            outputs = obj.attr('instObjGroups').outputs()
            if not outputs:
                offence_count += 1
                self.append_to_warnings('No shading group assigned at object level: ' + obj.name(), 2)
                self.append_to_hilight_list(obj)
            else:
                for o in outputs:
                    if type(o) == pm.nt.ShadingEngine:
                        if not o == ig:
                            offence_count += 1
                            self.append_to_warnings('Object assigned to non default shader: ' + obj.name(), 2)
                            self.append_to_hilight_list(obj)
            object_count += 1
            self.update_progress(object_count)
        if offence_count:
            self.append_to_warnings(
                str(offence_count) + ' objects are not exclusively assigned the default shader')
            will_fix = True
        else:
            self.append_to_infos(
                'All objects are assigned the default shader already')
        return will_fix

    def fix(self, objects=None, **kwargs):
        objects = su.get_geometry(objects)
        if not objects:
            self.append_to_errors('Nothing selected')
            return False

        num_objects = len(objects)
        sg = pm.PyNode('initialShadingGroup')
        pm.sets(sg, edit=True, forceElement=objects)
        self.append_to_infos(
            str(num_objects) + ' have been assigned the default shading group')


    def label(self):
        return 'Non-default Shaders'

    def author(self):
        return {'name': 'Julian Mann', 'email': 'julian.mann@gmail.com'}
