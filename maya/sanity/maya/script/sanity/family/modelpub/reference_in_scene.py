import pymel.core as pm
import sanity.core.check as sc
import pymel.core.uitypes as gui
import sanity.core.util as su

label_tip = """No references should be present in published model scenes.
They create unwanted clutter for downstream artists."""

check_tip = "Check for the presence of referenced geometry"

fix_tip = "Give option to remove all, import all, or open ref editor."


class Klass(sc.SanityCheck):

    def __init__(self, **kwargs):
        super(Klass, self).__init__(**kwargs)

    def check(self, objects=None, **kwargs):
        will_fix = False
        refs = pm.getReferences()
        offence_count = 0
        if refs:
            for ref in refs:
                offence_count += 1
                self.append_to_warnings('Reference : ' + str(
                    ref) + '  from reference file: ' + str(refs[ref]), 2)
        if offence_count:
            will_fix = True
            self.append_to_warnings('Found ' + str(offence_count) + ' references')
        else:
            self.append_to_infos('No references in the scene')
        return will_fix

    def fix(self, objects=None, **kwargs):
        """Give option to remove all, import all, or open ref editor.
        """
        win = gui.Window()
        win.setTitle('Fix References')
        win.setIconName('Fix References')
        win.setWidthHeight((300, 200))
        column = pm.columnLayout()
        column.adjustableColumn(True)

        pm.button(
            label='Remove All',
            annotation='Remove all references in the scene',
            command=pm.Callback(self.on_remove_refs_button_click, win)
        )
        pm.button(
            label='Import All',
            annotation='Import all references in the scene',
            command=pm.Callback(self.on_import_refs_button_click, win)
        )
        pm.button(
            label='Reference Editor',
            annotation='Open the reference editor',
            command=pm.Callback(self.on_edit_refs_button_click, win)
        )
        win.show()
        win.setResizeToFitChildren()

    def on_remove_refs_button_click(self, win):
        refs = pm.getReferences()
        if refs:
            for ref in refs:
                ref_name = str(refs[ref])
                refs[ref].remove()
                pm.displayInfo('Removed ' + ref_name)
        pm.deleteUI(win, window=True)

    def on_import_refs_button_click(self, win):
        refs = pm.getReferences()
        if refs:
            for ref in refs:
                ref_name = str(refs[ref])
                refs[ref].importContents()
                pm.displayInfo('imported ' + ref_name)
        pm.deleteUI(win, window=True)

    def on_edit_refs_button_click(self, win):
        pm.mel.ReferenceEditor()
        pm.deleteUI(win, window=True)

    def label(self):
        return 'References Present'

    def author(self):
        return {'name': 'Julian Mann', 'email': 'julian.mann@gmail.com'}
