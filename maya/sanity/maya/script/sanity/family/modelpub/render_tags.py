import pymel.core as pm
import sanity.core.check as sc
import sanity.core.util as su


label_tip = """Published models should have sensible render tag defaults:
* doubleSided Off
* opposite Off
* smoothShading On
* motionBlur On
* visibleInReflections On
* castsShadows On
* receiveShadows On
* primaryVisibility On"""

check_tip = "Check for non-default render tags"

fix_tip = "Set render flags to their defaults."


class Klass(sc.SanityCheck):

    RENDER_FLAGS = [
        ('doubleSided', False),
        ('opposite', False),
        ('smoothShading', True),
        ('motionBlur', True),
        ('visibleInReflections', True),
        ('castsShadows', True),
        ('receiveShadows', True),
        ('primaryVisibility', True)
    ]

    def __init__(self, **kwargs):
        super(Klass, self).__init__(**kwargs)

    def check(self, objects=None, **kwargs):
        objects = su.get_geometry(objects)
        if not objects:
            self.append_to_errors('No mesh objects provided')
            return False
        will_fix = False
        object_count = 0
        offence_count = 0
        num_objects = len(objects)
        self.start_progress(num_objects)
        for obj in objects:
            for att, flag in self.RENDER_FLAGS:
                attr = obj.attr(att)
                val = attr.get()
                if val != flag:
                    offence_count += 1
                    self.append_to_warnings('Bad value: ' + attr.name(
                    ) + ': ' + str(val) + '. should be: ' + str(flag), 2)
                    self.append_to_hilight_list(obj)
            object_count += 1
            self.update_progress(object_count)
        if offence_count:
            self.append_to_warnings(
                str(offence_count) + ' render flag are not default on selected objects')
            will_fix = True
        else:
            self.append_to_infos(
                'All selected objects have expected render flag values')
        return will_fix

    def fix(self, objects=None, **kwargs):
        objects = su.get_geometry(objects)
        if not objects:
            self.append_to_errors('No mesh objects provided')
            return
        object_count = 0
        fix_count = 0
        num_objects = len(objects)
        self.start_progress(num_objects)
        for obj in objects:
            something_fixed = False
            for att, flag in self.RENDER_FLAGS:
                attr = obj.attr(att)
                if attr.get() != flag:
                    attr.set(flag)
                    something_fixed = True
                    self.append_to_infos(attr.name() + ' set to ' + str(flag), 2)
            if something_fixed:
                fix_count += 1
            object_count += 1
            self.update_progress(object_count)

        self.append_to_infos(str(
            fix_count) + ' objects had render flags reset to their defaults')

    def label(self):
        return 'Bad Render Tags'

    def author(self):
        return {'name': 'Julian Mann', 'email': 'julian.mann@gmail.com'}
