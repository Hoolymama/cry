import pymel.core as pm
import sanity.core.check as sc
import sanity.core.util as su


label_tip = """Unused shaders should be deleted before publishing.
They create unwanted clutter in the hypershade for downstream artists.

This check and fix functions both operate on the whole scene."""

check_tip = """Check for unused shader related issues:
Specifically:
* Unused shaders.
* initialShadingGroup with no connection from lambert1.
* lambert1 color other than mid grey and non-zero ransparency.
* mapped attributes on lambert1."""


fix_tip = """Delete all unused shaders wiuth MLdeleteUnused().

Only non-referenced shaders will be deleted.

If the initial shading group does not have the default shader attached
with the default attributes, that will be fixed too."""


class Klass(sc.SanityCheck):

    def __init__(self, **kwargs):
        super(Klass, self).__init__(**kwargs)

    def check(self, objects=None, **kwargs):

        will_fix = False
        nodes = self.__unused_shading_nodes()
        unused_offence_count = 0
        if nodes:
            unused_offence_count = len(nodes)
            self.append_to_warnings('Unused Shaders: ', 2)
            self.append_to_warnings(nodes, 2)
            self.append_to_warnings(
                'Other unused nodes may be found recursively during the fix function', 2)
            self.append_to_hilight_list(nodes)
        ig_issues = False
        ig = pm.PyNode('initialShadingGroup')
        lambert = pm.PyNode('lambert1')
        if not ig in lambert.attr('outColor').outputs():
            self.append_to_warnings(
                'Lambert1 is not connected to initialShadingGroup', 2)
            ig_issues = True
        if lambert.attr('color').get() != (0.5, 0.5, 0.5):
            self.append_to_warnings('Lambert1 is not mid grey', 2)
            ig_issues = True
        if lambert.attr('transparency').get() != (0.0, 0.0, 0.0):
            self.append_to_warnings('Lambert1 is not opaque', 2)
            ig_issues = True
        if lambert.inputs():
            for i in lambert.inputs():
                self.append_to_warnings('Lambert1 has mapped attributes', 2)
            ig_issues = True
        if self.has_warnings_or_errors():
            if unused_offence_count:
                self.append_to_warnings(
                    'There are at least ' + str(unused_offence_count) + ' unused shading nodes')
            if ig_issues:
                self.append_to_warnings('Initial shading group and lambert1 are not in their default state')
            will_fix = True
        else:
            self.append_to_infos(
                'No unused shaders and nothing is wrong with lambert1')
        return will_fix

    def fix(self, objects=None, **kwargs):

        pm.mel.MLdeleteUnused()

        ig = pm.PyNode('initialShadingGroup')
        lambert = pm.PyNode('lambert1')
        if not ig in lambert.attr('outColor').outputs():
            lambert.attr('outColor') >> ig.attr('surfaceShader')
            self.append_to_infos('Connected: lambert1 to initialShadingGroup')
        if not lambert.attr('color').get() == (0.5, 0.5, 0.5):
            lambert.attr('color').set(0.5, 0.5, 0.5)
            self.append_to_infos('Set lambert1 color to mid grey')
        if not lambert.attr('transparency').get() == (0.0, 0.0, 0.0):
            lambert.attr('transparency').set(0.0, 0.0, 0.0)
            self.append_to_infos('Set lambert1 to be opaque')
        conns = pm.listConnections(lambert, source=True, destination=False,
                                   plugs=True, connections=True)
        for c in conns:
            c[1] // c[0]
            c1_name = c[1].node().name()
            did_delete = pm.mel.deleteIfNotReferenced(c[1].node())
            if did_delete:
                self.append_to_infos(
                    'Disconnected and deleted ' + c1_name + ' from lambert1')

    def label(self):
        return 'Unused Shaders'

    def author(self):
        return {'name': 'Julian Mann', 'email': 'julian.mann@gmail.com'}

    # private

    def __shader_has_use(self, shader):
        conns = pm.listConnections(shader, connections=True,
                                   plugs=True, source=False, destination=True)
        for conn in conns:
            if conn[0] != pm.Attribute(shader.attr('message')):
                return True
        return False

    def __unused_shading_groups(self):
        result = []
        sets = pm.ls(sets=True)
        for the_set in sets:
            if pm.mel.shadingGroupUnused(the_set):
                result.append(the_set)
        return result

    def __unused_materials(self):
        result = []
        materials = pm.ls(mat=True)
        for mat in materials:
            if not self.__shader_has_use(mat):
                result.append(mat)
        return result

    def __unused_render_nodes(self):
        result = []
        type_tex = pm.listNodeTypes('texture')
        type_uti = pm.listNodeTypes('utility')
        type_imp = pm.listNodeTypes('imageplane')
        type_shd = pm.listNodeTypes('shader')
        type_all = list(set(type_tex + type_uti + type_imp + type_shd))
        render_nodes = pm.ls(type=(type_all))
        for node in render_nodes:
            if not self.__shader_has_use(node):
                result.append(node)
        return result

    def __unused_shading_nodes(self):
        result = self.__unused_shading_groups()
        result = result + self.__unused_materials()
        result = result + self.__unused_render_nodes()
        return list(set(result))
