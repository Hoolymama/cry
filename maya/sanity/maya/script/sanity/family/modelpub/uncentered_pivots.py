

import pymel.core as pm
import sanity.core.check as sc
import sanity.core.util as su

label_tip = """Published models should have sensible pivot defaults:
Specifically
* centered pivots on all transforms except the top level group.
* zero pivot on top level transform.
* frozen transforms"""

check_tip = """Check all objects have centered pivots:
Specifically:
* non-zero pivots on assemblies
* uncentered pivots on other transforms
* non-zero translations
* non-zero rotations
* non-unit scale

Check hierarchy top-down.
Offending objects will be added
to the hilight list."""

fix_tip = """Center pivots and freeze transforms."""

class Klass(sc.SanityCheck):

    def __init__(self, **kwargs):
        super(Klass, self).__init__(**kwargs)

    def bad_assembly(self, obj):
        for att, n in [
            ('rotatePivot', 0.0),
            ('scalePivot', 0.0),
            ('rotatePivotTranslate', 0.0),
            ('scalePivotTranslate', 0.0),
            ('translate', 0.0),
            ('rotate', 0.0),
                ('scale', 1.0)]:
            p = obj.attr(att).get()
            for el in p:
                if el != n:
                    return True
        return False

    # first, check the should-be-zero stuff.
    # Then we compare the ws pivot position to the
    # the bb center.
    # As we will only be calling this function
    # on objects that are below objects with identity transforms,
    # its as if we are in worldspace. Therefore we can safely
    # use object space functions and know they are in world space
    def bad_child(sel, obj):
        for att, n in [
            ('rotatePivotTranslate', 0.0),
            ('scalePivotTranslate', 0.0),
            ('translate', 0.0),
            ('rotate', 0.0),
                ('scale', 1.0)]:
            p = obj.attr(att).get()
            for el in p:
                if el != n:
                    return True

        bb = obj.getBoundingBox()
        c = bb.center()
        piv = obj.getRotatePivot()
        if pm.datatypes.Vector(c - piv).length() > 0.00001:
            return True
        # t = obj.attr('translate').get()
        # if pm.datatypes.Vector(t - piv).length() > 0.00001:
        #     return True
        return False

    # Recursively find bad children.
    # Prune at bad child, because if a child is bad,
    # we already know its kids will be bad. (just like real life)
    def find_bad_children(self, obj, result):
        children = obj.getChildren()
        for child in children:
            if type(child) == pm.nt.Transform:
                if self.bad_child(child):
                    result.append(child)
                else:
                    self.find_bad_children(child, result)

    def check(self, objects=None, **kwargs):

        objs = su.get_transforms(objects)
        if not objs:
            self.append_to_errors('No transforms selected')
            return False


        will_fix = False
        assemblies = []
        for obj in objs:
            parents = obj.getAllParents()
            if not parents:
                assemblies.append(obj)
            else:
                assemblies.append(obj.getAllParents()[-1])

        # uniquify
        assemblies = list(set(assemblies))
        object_count = 0
        offence_count = 0
        num_objects = len(assemblies)
        self.start_progress(num_objects)
        for obj in assemblies:
            if self.bad_assembly(obj):
                self.append_to_hilight_list(obj)
                offence_count += 1
                self.append_to_warnings(
                    'Assembly transform needs sanitizing: ' + obj.name(), 2)
                continue

            bad_children = []
            self.find_bad_children(obj, bad_children)
            if bad_children:
                self.append_to_hilight_list(bad_children)
                offence_count += len(bad_children)
                self.append_to_warnings(
                    'Children of ' + obj.name() + ' need sanitizing:', 2)
                self.append_to_warnings(bad_children, 2)
            else:
                self.append_to_infos(obj.name() + ' and all its children have frozen transforms and good pivots')
            object_count += 1
            self.update_progress(object_count)
        if offence_count:
            self.append_to_warnings(str(offence_count) + ' objects and their children need transforms frozen and pivots centered.')
            will_fix = True
        else:
            self.append_to_infos('All selected assemblies and their children already have frozen transforms and centered pivots')
        return will_fix


    def make_identity(self, obj):
        try:
            pm.makeIdentity(obj, apply=True)
            return True
        except:
            return False

    def fix(self, objects=None, **kwargs):
        objs = su.get_transforms(objects)
        if not objs:
            self.append_to_errors('No transforms selected')
            return

        assemblies = []
        for obj in objs:
            parents = obj.getAllParents()
            if not parents:
                assemblies.append(obj)
            else:
                assemblies.append(obj.getAllParents()[-1])
        # uniquify
        assemblies = list(set(assemblies))
        object_count = 0
        child_count = 0
        num_objects = len(assemblies)
        self.start_progress(num_objects)
        for obj in assemblies:
            if self.make_identity(obj):
                obj.setPivots((0, 0, 0))
                children = pm.ls(obj, dag=True, type='transform')[1:]
                if children:
                    child_count += len(children)
                    for child in children:
                        child.centerPivots()
                        self.append_to_infos(
                            'Centered pivots for ' + child.name(), 2)
            else:
                self.append_to_errors('Make Identity Failed! maybe some items are locked')
                self.append_to_errors('Run the Incoming Connections check then try again')

            object_count += 1
            self.update_progress(object_count)

        self.append_to_infos(
            str(object_count) + ' assemblies and ' + str(child_count) +
            ' children now have transforms frozen and pivots centered')

    def label(self):
        return 'Uncentered Pivots'

    def author(self):
        return {'name': 'Julian Mann', 'email': 'julian.mann@gmail.com'}
