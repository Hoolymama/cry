import pymel.core as pm
import sanity.core.check as sc
import sanity.core.util as su

label_tip = """Construction history should be deleted before publishing.
Construction history can cause unexpected problems for artists
downstream in the pipleline. These include issues related to plugins
and 3rd party scripts. In addition, some history may fail for
example when an animator transforms an asset."""

check_tip = "Check for polys with history."

fix_tip = "Delete history on selected."


class Klass(sc.SanityCheck):

    def __init__(self, **kwargs):
        super(Klass, self).__init__(**kwargs)

    def check(self, objects=None, **kwargs):
        objects = su.get_geometry(objects)
        if not objects:
            self.append_to_errors('Nothing selected')
            return False

        will_fix = False
        object_count = 0
        offence_count = 0
        num_objects = len(objects)
        self.start_progress(num_objects)
        for obj in objects:
            hist = pm.listHistory(obj)
            if len(hist) > 1:
                self.append_to_hilight_list(obj)
                self.append_to_warnings('Object has history: ' + obj.name(), 2)
                offence_count += 1
            object_count += 1
            self.update_progress(object_count)
        if offence_count:
            self.append_to_warnings(
                str(offence_count) + ' objects with construction history')
            will_fix = True
        else:
            self.append_to_infos('No objects with construction history')
        return will_fix

    def fix(self, objects=None, **kwargs):
        objects = su.get_geometry(objects)
        if not objects:
            self.append_to_errors('Nothing selected')
            return False

        num_objects = len(objects)
        self.start_progress(num_objects)
        object_count = 0
        fail_count = 0
        for obj in objects:
            pm.delete(obj, ch=True)
            hist = pm.listHistory(obj)
            if len(hist) > 1:
                fail_count += 1
                self.append_to_infos(
                    'Failed to delete history for ' + obj.name(), 2)
            else:
                self.append_to_infos('Deleted history for ' + obj.name(), 2)
            object_count += 1
            self.update_progress(object_count)
        self.append_to_infos('Construction history deleted for ' +
                          str(object_count - fail_count) + ' objects')
        if fail_count:
            self.append_to_infos(
                'Failed to delete history for ' +
                str(fail_count) + ' objects. ' +
                'Check shader assignments')

    def label(self):
        return 'Construction History'

    def author(self):
        return {'name': 'Julian Mann', 'email': 'julian.mann@gmail.com'}
