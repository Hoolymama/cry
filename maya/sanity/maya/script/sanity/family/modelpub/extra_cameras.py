import pymel.core as pm
import sanity.core.check as sc
import sanity.core.util as su

label_tip = """Extra cameras should be deleted before publishing.
They create unwanted clutter in the scene for downstream artists.

This check does not require a selection. It operates on the whole scene.
Likewise, the fix ignores the selection. It deletes any extra cameras
found by the check method.

Extra cameras are any camersas that are not startup cameras, i.e.:
persp, top, front, right"""

check_tip = "Check for the cameras that are not startup cameras."

fix_tip = "Delete all cameras that are not startup cameras."


class Klass(sc.SanityCheck):
 
    def __init__(self, **kwargs):
        super(Klass, self).__init__(**kwargs)

    def check(self, objects=None, **kwargs):
        offence_count = 0
        will_fix = False
        cameras = pm.ls(cameras=True)
        if cameras:
            for cam in cameras:
                if not cam.getStartupCamera():
                    offence_count += 1
                    self.append_to_hilight_list(cam)
                    self.append_to_warnings('Not startup camera: '+cam.name(),2)

        if offence_count:
            self.append_to_warnings(
                str(offence_count) + ' superfluous cameras found')
            will_fix = True
        else:
            self.append_to_infos('No superfluous cameras in the scene')
        return will_fix

    def fix(self, objects=None, **kwargs):
        
        offence_count = len(self.hilight_list)
        pm.delete(self.hilight_list)
        self.append_to_infos(str(offence_count) + ' superfluous cameras deleted')

    def label(self):
        return 'Superfluous Cameras'

    def author(self):
        return {'name': 'Julian Mann', 'email': 'julian.mann@gmail.com'}
