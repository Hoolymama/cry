import re
import pymel.core as pm
import sanity.core.check as sc
import pymel.core.uitypes as gui

label_tip = "Published model names should adhere to a naming convention"

check_tip = "Check selected assemblies for bad naming."

fix_tip = "Use matchRenameDoIt to fix bad naming in assemblies of selected objects."



class Klass(sc.SanityCheck):

    def __init__(self, **kwargs):
        super(Klass, self).__init__(**kwargs)

    def check(self, objects=None, **kwargs):

        will_fix = False
        if not objects:
            self.append_to_errors('No objects selected')
            return False
        assemblies = []
        for obj in objects:
            try:
                parents = obj.getAllParents()
                if not parents:
                    assemblies.append(obj)
                else:
                    assemblies.append(obj.getAllParents()[-1])
            except:
                self.append_to_errors('This check currently works on dag nodes. ' + obj.name() + ' will be skipped')
        assemblies = list(set(assemblies))

        # pre count to set progress window
        object_count = 0
        offence_count = 0
        num_objects = 0
        for assembly in assemblies:
            dag = pm.ls(assembly, dag=True)
            num_objects += len(dag)
        self.start_progress(num_objects)

        # regex to check naming convention
        matcher = re.compile(
            '^[a-z]([a-z_]*[a-z])?_[0-9]{4}_(GRP|SHP|GEO)$')
        for assembly in assemblies:
            dag = pm.ls(assembly, dag=True)
            for node in dag:
                name = node.shortName()
                if not matcher.match(name):
                    offence_count += 1
                    self.append_to_hilight_list(node)
                    self.append_to_warnings('Unconventional name: ' + name, 2)
                object_count += 1
                self.update_progress(object_count)
        if offence_count:
            self.append_to_warnings(
                str(offence_count) + ' objects have unconventional names')
            will_fix = True
        else:
            self.append_to_infos('No unconventional names found')
        return will_fix





    def fix(self, objects=None, **kwargs):

        win = gui.Window()
        win.setTitle('Rename')
        win.setIconName('Rename')
        win.setWidthHeight((300, 200))
        column = pm.columnLayout()
        column.adjustableColumn(True)

        pm.button(
            label='Maya Search Replace',
            annotation='Open UI for the standard maya search replace tool',
            command=pm.Callback(self.on_maya_search_replace_click, win)
        )
        pm.button(
            label='Medusa Search Replace',
            annotation='Open UI for the powerful search replace tool in Medusa',
            command=pm.Callback(self.on_medusa_search_replace_click, win)
        )
        pm.button(
            label='Auto rename',
            annotation='Attempt auto rename',
            command=pm.Callback(self.on_medusa_auto_rename_click, win, objects)
        )
        win.show()
        win.setResizeToFitChildren()

    def on_maya_search_replace_click(self, win):
        pm.mel.performSearchReplaceNames(1);
        pm.deleteUI(win, window=True)

    def on_medusa_search_replace_click(self, win):
        pm.mel.matchRenameUI()
        pm.deleteUI(win, window=True)

    def on_medusa_auto_rename_click(self, win, objects):
        if not objects:
            self.append_to_errors('No objects selected')
            return

        assemblies = []
        for obj in objects:
            parents = obj.getAllParents()
            if not parents:
                assemblies.append(obj)
            else:
                assemblies.append(obj.getAllParents()[-1])
        assemblies = list(set(assemblies))

        dags = pm.ls(assemblies, dag=True)
        shapes = pm.ls(assemblies, dag=True, v=True, ni=True, shapes=True)
        geos = pm.ls(assemblies,
                    dag=True, v=True, ni=True, leaf=True,
                    type=('mesh', 'subdiv', 'nurbsSurface'))

        object_count = 0
        num_objects = (len(dags)*4) + len(shapes) + len(geos)
        self.start_progress(num_objects)

        # rename objects with 6 steps of matchRename

        # underscorize everything - selected and below
        for node in dags:
            pm.mel.matchRenameObject(node, '', '', 2, 7, 0, 4)
            object_count += 1
            self.update_progress(object_count)
        # replace 0 or mmore underscores at the end of a line
        # with a single underscore -  selected and below
        for node in dags:
            pm.mel.matchRenameObject(node, '_*$', '_', 2, 1, 0, 4)
            object_count += 1
            self.update_progress(object_count)
        # strip away the end of the string from the first number
        # if the number is preceded by 0 or more underscores,
        # replace them with a single underscore
        # add a new unique 4 padded number
        # give shapes the same name as their parents
        # selected and below
        for node in dags:
            pm.mel.matchRenameObject(node, '_*[0-9].*', '_', 4, 1, 1, 4)
            object_count += 1
            self.update_progress(object_count)
        # add the suffix _GRP to all transforms
        for node in dags:
            pm.mel.matchRenameObject(node, '$', '_GRP', 2, 1, 0, 4)
            object_count += 1
            self.update_progress(object_count)
        # add the suffix _SHP to all shapes
        for node in shapes:
            pm.mel.matchRenameObject(node, '$', '_SHP', 1, 1, 0, 4)
            object_count += 1
            self.update_progress(object_count)
        # add the suffix _GEO to all shapes that are geometry
        for node in geos:
            pm.mel.matchRenameObject(node, '_SHP$', '_GEO', 1, 1, 0, 4)
            object_count += 1
            self.update_progress(object_count)

        self.append_to_infos(
            'Fixed unconventional names for ' + str(len(dags)) + ' objects')
        self.append_to_infos(assemblies)
        # we need to end the progress window explicitly
        self.end_progress()
        self.clear_hilight_list()
        ui = self.get_ui()
        if ui:
            ui.select_and_enable_hilight_list()
            ui.set_status_light('auto')
            ui.display_output()

        pm.deleteUI(win, window=True)

    # def fix(self, objects=None, **kwargs):

    #     if not objects:
    #         self.append_to_errors('No objects selected')
    #         return

    #     assemblies = []
    #     for obj in objects:
    #         parents = obj.getAllParents()
    #         if not parents:
    #             assemblies.append(obj)
    #         else:
    #             assemblies.append(obj.getAllParents()[-1])
    #     assemblies = list(set(assemblies))

    #     dags = pm.ls(assemblies, dag=True)
    #     shapes = pm.ls(assemblies, dag=True, v=True, ni=True, shapes=True)
    #     geos = pm.ls(assemblies,
    #                 dag=True, v=True, ni=True, leaf=True,
    #                 type=('mesh', 'subdiv', 'nurbsSurface'))

    #     object_count = 0
    #     num_objects = (len(dags)*4) + len(shapes) + len(geos)
    #     self.start_progress(num_objects)

    #     # rename objects with 6 steps of matchRename

    #     # underscorize everything - selected and below
    #     for node in dags:
    #         pm.mel.matchRenameObject(node, '', '', 2, 7, 0, 4)
    #         object_count += 1
    #         self.update_progress(object_count)
    #     # replace 0 or mmore underscores at the end of a line
    #     # with a single underscore -  selected and below
    #     for node in dags:
    #         pm.mel.matchRenameObject(node, '_*$', '_', 2, 1, 0, 4)
    #         object_count += 1
    #         self.update_progress(object_count)
    #     # strip away the end of the string from the first number
    #     # if the number is preceded by 0 or more underscores,
    #     # replace them with a single underscore
    #     # add a new unique 4 padded number
    #     # give shapes the same name as their parents
    #     # selected and below
    #     for node in dags:
    #         pm.mel.matchRenameObject(node, '_*[0-9].*', '_', 4, 1, 1, 4)
    #         object_count += 1
    #         self.update_progress(object_count)
    #     # add the suffix _GRP to all transforms
    #     for node in dags:
    #         pm.mel.matchRenameObject(node, '$', '_GRP', 2, 1, 0, 4)
    #         object_count += 1
    #         self.update_progress(object_count)
    #     # add the suffix _SHP to all shapes
    #     for node in shapes:
    #         pm.mel.matchRenameObject(node, '_GRP$', '_SHP', 1, 1, 0, 4)
    #         object_count += 1
    #         self.update_progress(object_count)
    #     # add the suffix _GEO to all shapes that are geometry
    #     for node in geos:
    #         pm.mel.matchRenameObject(node, '_GRP$', '_GEO', 1, 1, 0, 4)
    #         object_count += 1
    #         self.update_progress(object_count)

    #     self.append_to_infos(
    #         'Fixed unconventional names for ' + str(len(dags)) + ' objects')
    #     self.append_to_infos(assemblies)

    def label(self):
        return 'Unconventional Names'

    def author(self):
        return {'name': 'Julian Mann', 'email': 'julian.mann@gmail.com'}
