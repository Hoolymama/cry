import pymel.core as pm
import sanity.core.check as sc
import sanity.core.util as su

label_tip = "Published models should have no incoming connections."

check_tip = """Check selected transforms for  incoming connections
such as animation, constraints, expressions, locked state, and so on."""

fix_tip = "Delete constraints, remove connections, unlock, and unhide."

class Klass(sc.SanityCheck):

    def __init__(self, **kwargs):
        super(Klass, self).__init__(**kwargs)


    def check(self, objects=None, **kwargs):

        will_fix = False
        objects = pm.ls(objects, transforms=True, dag=True)
        if not objects:
            self.append_to_errors('No transforms selected')
            return False
        object_count = 0
        offence_count = 0
        num_objects = len(objects)
        self.start_progress(num_objects)
        for obj in objects:
            # dont test constraints
            # however, do add them to the hilight list
            # so they can be deleted
            if su.is_constraint(obj):
                self.append_to_hilight_list(obj)
                offence_count += 1
                self.append_to_warnings(obj.name() + ' is a constraint', 2)
            else:
                connection_offence = False
                locked_offence = False
                hidden_offence = False
                non_keyable_offence = False
                for att in su.KEYABLE_ATTS:
                    attr = obj.attr(att)
                    if attr.isLocked():
                        offence_count += 1
                        locked_offence = True
                    if not attr.isKeyable():
                        offence_count += 1
                        non_keyable_offence = True
                    if attr.isHidden():
                        offence_count += 1
                        hidden_offence = True

                conns = pm.listConnections(obj, connections=True, plugs=True, destination=False, source=True)
                if conns:
                    offence_count += len(conns)
                    connection_offence = True

                if connection_offence or locked_offence or hidden_offence or non_keyable_offence:
                    msg = obj.name() + ' : '
                    if connection_offence:
                        msg += '(has connections) '
                    if locked_offence:
                        msg += '(has locked attributes) '
                    if hidden_offence:
                        msg += '(has hidden attributes) '
                    if non_keyable_offence:
                        msg += '(has non keyable attributes) '
                    self.append_to_hilight_list(obj)
                    self.append_to_warnings(msg, 2)

            object_count += 1
            self.update_progress(object_count)

        if offence_count:
            self.append_to_warnings(str(offence_count) + ' attribute issues')
            will_fix = True
        else:
            self.append_to_infos(
                'No transform attributes are locked, hidden, or have incoming connections')
        return will_fix

    def fix(self, objects=None, **kwargs):

        objects = pm.ls(objects, transforms=True)
        if not objects:
            self.append_to_errors('No transforms selected')
            return

        object_count = 0
        num_objects = len(objects)
        self.start_progress(num_objects)
        offence_count = 0
        constraint_count = 0
        for obj in objects:
            obj_name = obj.name()
            if su.is_constraint(obj):
                pm.delete(obj)
                self.append_to_infos('Removed constraint: ' + obj_name, 2)
                constraint_count += 1
            else:
                offence_fixed = False
                for att in su.KEYABLE_ATTS:
                    attr = obj.attr(att)
                    if attr.isLocked():
                        attr.unlock()
                        offence_fixed = True
                    if not attr.isKeyable():
                        attr.setKeyable(True)
                        offence_fixed = True
                    if attr.isHidden():
                        attr.channelBox(True)
                        attr.setKeyable(True)
                        offence_fixed = True
                if offence_fixed:
                    offence_count += 1
                    self.append_to_infos('Fixed attribute state for: ' + obj_name, 2)

                conns = pm.listConnections(obj, connections=True, plugs=True,
                                           destination=False, source=True)

                for conn in conns:
                    src, dest = conn[1].name(), conn[0].name()
                    conn[1] // conn[0]
                    self.append_to_infos(
                        'Disconnected: ' + src + ' from ' + dest, 2)
            object_count += 1
            self.update_progress(object_count)
        self.append_to_infos(str(constraint_count) + ' constraints removed.')
        self.append_to_infos(str(offence_count) + ' inputs cleaned.')

    def label(self):
        return 'Incoming Connections'

    def author(self):
        return {'name': 'Julian Mann', 'email': 'julian.mann@gmail.com'}
