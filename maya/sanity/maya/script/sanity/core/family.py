import sys
import pymel.core as pm
#  import sanity.core as sc

from sanity.core import aggregator as agg
from sanity.core import checkui as cui


class Family():

    def __init__(self, pkg, tabs):
        pm.setParent(tabs)
        self.package_name = pkg
        full_mod = 'sanity.family.' + self.package_name
        self.module = __import__(full_mod, fromlist=['*'])
        self.column = pm.columnLayout()
        self.column.adjustableColumn(True)
        family_label = self.module.LABEL
        self.check_uis = []
        for submod in self.module.__all__:
            key = self.module.__name__ + '.' + submod
            submod = sys.modules[key]
            klass = getattr(submod, 'Klass')
            pm.setParent(self.column)
            k = klass()
            ui = cui.SanityCheckUI(k)
            k.set_ui(ui)
            self.check_uis.append(ui)

        pm.setParent(self.column)
        agg_object = agg.Klass(checkuis=self.check_uis, family_label=family_label)
        ui = cui.SanityCheckAllUI(agg_object)
        agg_object.set_ui(ui)
        self.aggregator_ui = ui
        tabs.setTabLabel((self.column, family_label))
        num = tabs.getNumberOfChildren()
        tabs.setSelectTabIndex(num)
        pm.setParent(tabs)

    def get_check_uis(self):
        return self.check_uis

    def get_aggregator_ui(self):
        return self.aggregator_ui