from sanity.core import check as sc

label_tip = """Runs all checks in this family at once.
It triggers check functions in the UI class of family checks,
and for that reason, THIS check is only valid when there
is a UI."""

check_tip = """Run checks marked active.
Loop over checks and run any whose checkbox is on.
If any fail, this check will fail, and therefore its light will show red.
Additionaly, if any failed checks have a list of objects to be fixed,
the accumulation of all those objects will be stored on this check and
the pick hilight list button will be enabled.
"""


class Klass(sc.SanityCheck):

    def __init__(self, **kwargs):
        super(Klass, self).__init__(**kwargs)
        self.checkuis = kwargs.get('checkuis', [])
        self.family_label = kwargs.get('family_label', '')

    def get_check_uis(self):
        return self.checkuis

    def check(self, objects=None, **kwargs):

        check_count = 0
        prefix_output = self.family_label + ' family: '

        for c in self.checkuis:
            if not c.activeCB.getValue():
                continue
            c.on_check_button_click(objects)

            if c.sanity_check.get_state() == 'failed':
                self.append_to_warnings(
                    prefix_output + ' ' + c.sanity_check.label() + ' failed')
            else:
                self.append_to_infos(prefix_output + ' ' + c.sanity_check.label() + ' passed')

            self.append_to_hilight_list(c.sanity_check.get_hilight_list())
            check_count += 1

        if not check_count:
            self.append_to_warnings(prefix_output + 'Nothing to check - use the checkboxes')

        if not self.has_warnings_or_errors():
            self.append_to_infos(prefix_output + ' All passed!')
        return False

    def label(self):
        return 'RUN ALL CHECKS'

    def author(self):
        return {'name': 'Julian Mann', 'email': 'julian.mann@gmail.com'}

    def before_reset_button_click(self):
        for c in self.checkuis:
            c.on_reset_button_click()
