import re
import sys

class SanityCheck(object):

    # parent class of any sanity check
    # fixable cannot be true if fix() not implemented
    def __init__(self, **kwargs):
        self._config = {}
        self._config['ui'] = False
        self._config['debug'] = kwargs.get('debug', False)
        #self._config['fixable'] = self.fix.im_func != SanityCheck.fix.im_func
        self._log = []
        self._hilight_list = []
        self._state = None
        self._progress = {
            'start_progress': self.default_start_progress,
            'update_progress': self.default_update_progress,
            'end_progress': self.default_end_progress
            }
        self._make_tips()

    def _make_tips(self):
        self.tips = {}
        attrs = [
            ('label',   "No tool tip"),
            ('check',   "No tool tip"),
            ('fix',     "No tool tip"),
            ('hilight', "Select offensive objects or components"),
            ('reset',   "Reset original selection")
            ]
        for attr in attrs:
            try:
                current_module = self.__module__.split('.')[-1]
                self.tips[attr[0]] = getattr(__import__(current_module), attr[0] + '_tip')
            except:
                self.tips[attr[0]] = attr[1]

    # all functions that are designed to be overridden
    # by a plugin author will be called from this method
    # so that if they fail, the user will be told to contact
    # the plugin author rather than be presented with gobbledegook
    # The plugin author would then run in debug mode where
    # exception will not be caught.
    def error_with_author(self, func, *args, **kwargs):
        if self._config['debug']:
            return func(*args, **kwargs)
        else:
            try:
                return func(*args, **kwargs)
            except:
                msg = (
                    'Errors in Plugin Implementation: ' + self.__module__ +
                    '::' + func.__name__ + '(). Please notify author: ' +
                    self.author()['name'] + ' - ' + self.author()['email']
                    )
                self.append_to_errors(msg)
                self._state = 'failed'

    def set_debug(self, value=False):
        self._config['debug'] = value

    def is_fixable(self):
        return hasattr(self, 'fix') and callable(getattr(self, 'fix'))

    def set_ui(self, ui):
        self._config['ui'] = True
        self.ui = ui

    def get_ui(self):
        if self._config['ui']:
            return self.ui
        else:
            return None

    # set the state to passed or failed or none
    # also has ability to work out the state from the
    # contents of logs
    def set_state(self, state=None):
        if state == 'passed' or state == 'failed':
            self._state = state
        elif state == 'auto':
            if not self.has_warnings_or_errors():
                self._state = 'passed'
            else:
                self._state = 'failed'
        else:
            self._state = None

    def get_state(self):
        return self._state

    # progress
    # provide a function to start progress (such as opening a progress window)
    # and a function to update progress (such as updating a progress window)
    # and a function to end progress (such as closing a progress window)
    # we also define some simple default procs here
    ######################################################
    def register_progress_functions(
            self, start_progress_func,
            update_progress_func,
            end_progress_func):
        self._progress['start_progress'] = start_progress_func
        self._progress['update_progress'] = update_progress_func
        self._progress['end_progress'] = end_progress_func

    def start_progress(self, todo):
        self._progress['todo'] = todo
        return self._progress['start_progress']()

    def update_progress(self, amount):
        return self._progress['update_progress'](amount)

    def end_progress(self):
        return self._progress['end_progress']()

    # some simple default progress funcs
    def default_start_progress(self):
        todo_str = str(self._progress['todo'])
        msg = self._progress['action'] + '... ' + self.label() + ' 0/' + todo_str
        print(msg + '\n')

    def default_update_progress(self, amount):
        todo_str = str(self._progress['todo'])
        msg = self._progress['action'] + '... ' + self.label() + ' ' + str(amount) + '/' + todo_str + '\n'
        print(msg + '\n')

    def default_end_progress(self):
        msg = self._progress['action'] + '... ' + self.label() + ' complete!\n'
        print(msg + '\n')
    ######################################################

    # run_check() runs the user defined check and returns
    # True if we can try to fix it, False otherwise.
    # We try to take care of other stuff that needs doing
    # to set up and finish a check
    def run_check(self, objects=None, **kwargs):
        self.clear_log()
        self.clear_hilight_list()
        self.set_state(None)
        self._progress['action'] = 'Checking'
        self.append_to_infos(('*' * 15) + ' Checking ' + self.label() + ('*' * 15))
        will_fix = self.error_with_author(self.check, objects, **kwargs)
        if will_fix:
            will_fix = self.is_fixable()
        else:
            will_fix = False
        self.end_progress()
        self.set_state('auto')
        return will_fix

    # run_fix() runs the user defined fix and returns
    # whatever the  user defined fix wants to return.
    # We try to take care of other stuff taht needs doing
    # to set up and clean up after a fix
    def run_fix(self, objects=None, **kwargs):
        self.clear_log()
        self._progress['action'] = 'Fixing'
        self.set_state(None)
        self.append_to_infos(('*' * 15) + ' Fixing ' + self.label() + ('*' * 15))
        result = self.error_with_author(self.fix, objects, **kwargs)
        self.end_progress()
        self.clear_hilight_list()
        return result

    # manage the log
    # verbosity values are 1,2,3 and so on...
    # When log output is requested, the caller gives a
    # verbosity level. All messages with verbosity
    # less than or equal to that value are returned.
    # In addition, caller can provide a list of message
    # levels to display. e.g. ['i','w'] to see info
    # and warning messages, ['e'] for just errors etc.
    # For optimization purposes, we limit verbosity to 3
    def clear_log(self):
        self._log = []

    def append_to_log(self, msg, level, verbose=1):
        if not isinstance(msg, list):
            msg = [msg]
        for m in msg:
            self._log.append((str(m), level, verbose))

    def append_to_infos(self, msg, verbose=1):
        self.append_to_log(msg, 'i', verbose)

    def append_to_warnings(self, msg, verbose=1):
        self.append_to_log(msg, 'w', verbose)

    def append_to_errors(self, msg, verbose=1):
        self.append_to_log(msg, 'e', verbose)

    def get_log(self, level='all', verbose=1):
        result = []
        if level == 'all':
            level = ['i', 'w', 'e']
        if level == ['i', 'w', 'e'] and verbose == 3:
            return self._log
        for entry in self._log:
            if entry[1] in level and entry[2] <= verbose:
                result.append(entry)
        return result

    def has_warnings_or_errors(self):
        if next((x for x in self._log if x[1] in ['w', 'e']), False):
            return True
        else:
            return False

    # the highlight list should be populated
    # during the check method. The objects or
    # components it contains should be the
    # offending objects.
    def clear_hilight_list(self):
        self._hilight_list = []

    def get_hilight_list(self):
        return list(set(self._hilight_list))

    def append_to_hilight_list(self, obj):
        if obj:
            if not isinstance(obj, list):
                obj = [obj]
            for o in obj:
                self._hilight_list.append(o)

    def get_camel_name(self):
        v = ('').join([x.title() for x in re.findall(r'[a-z,0-9,A-Z]+', self.__module__)])
        return v[0].lower() + v[1:]

    # def is_checking(self):
    #     return self._progress['action'] == 'Checking'

    # def is_fixing(self):
    #     return self._progress['action'] == 'Fixing'

    def get_progress_action(self):
        return self._progress['action']

    def get_progress_todo(self):
        return self._progress['todo']

    # overrides to be provided by plugin developer
    def label(self):
        "Must override this method."
        raise NotImplementedError

    def author(self):
        "Must override this method."
        raise NotImplementedError

    def check(self):
        "Must override this method."
        raise NotImplementedError

    # def fix(self):
    #     """
    #     No fix implementation exists for this check.
    #     """
    #     pass

    # Below are some hooks that allow the author of a check to
    # hook into the lifecycle of some ui operations.
    #
    # There is probably a smarter way to do this that involves
    # overloading functions on the ui object instead of the check,
    # but it would make things less explicit and more complicated for
    # a plugin author.
    #
    # These will only get called by the UI of course.
    #
    # before reset hook
    def before_reset_button_click(self):
        """
        allow user to do stuff before reset.
        """
        pass

    # after reset hook
    def after_reset_button_click(self):
        """
        allow user to do stuff after reset.
        """
        pass

    # after select hilight_list hook
    # this is called when the highlight_list is
    # selected either after a check, or after
    # the pick_hilight_list button is pressed
    def after_select_hilight_list(self):
        """
        allow user to do stuff before pick hilight list.
        """
        pass

