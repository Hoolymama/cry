import os.path
# import sys
import pymel.core as pm
import pymel.core.uitypes as gui
from sanity.core import family as sfm
from sanity.core import util as su

# class SanityWindow is responsible for building the UI
# that will contain all tests. It is inherited from a PyMel Window.

main_sanity_window = 'does_not_exist'


def unique_sanity_window():
    global main_sanity_window
    if pm.window(main_sanity_window, query=True, exists=True):
        pm.deleteUI(main_sanity_window)
    main_sanity_window = SanityWindow()


class SanityWindow(gui.Window):

    DEBUG = True

    def __init__(self):
        self.setTitle('Sanity')
        self.setIconName('Sanity')
        self.setWidthHeight([500, 500])
        self.families = []
        self.form = pm.formLayout(numberOfDivisions=100)
        self.tabs = pm.tabLayout(
            scrollable=True,
            childResizable=True,
            selectCommand=pm.Callback(self.on_tab_changed)
            )
        pm.setParent(self.form)
        self.config_row = pm.rowColumnLayout(numberOfRows=1, rowHeight=(1, 24))
        self.config_buttons = {}
        self.create_config_buttons()
        self.initialize_config_buttons()
        pm.setParent(self.form)
        self.create_buttons()
        self.show()
        self.populate()
        self.setResizeToFitChildren()

    def create_config_buttons(self):
        pm.setParent(self.config_row)
        self.config_buttons['verbose'] = pm.iconTextRadioCollection()
        pm.iconTextRadioButton(
            style='iconOnly', width=24,
            image='sanity_verbose1_off.png',
            selectionImage='sanity_verbose1_on.png',
            ann='verbose level 1',
            onCommand=pm.Callback(self.on_verbosity_button_click, 1)
            )
        pm.iconTextRadioButton(
            style='iconOnly', width=24,
            image='sanity_verbose2_off.png',
            selectionImage='sanity_verbose2_on.png',
            ann='verbose level 2',
            onCommand=pm.Callback(self.on_verbosity_button_click, 2)
            )
        pm.iconTextRadioButton(
            style='iconOnly', width=24,
            image='sanity_verbose3_off.png',
            selectionImage='sanity_verbose3_on.png',
            ann='verbose level 3',
            onCommand=pm.Callback(self.on_verbosity_button_click, 3)
            )
        pm.setParent(self.config_row)
        pm.separator(style='double', horizontal=False)
        self.config_buttons['debug'] = pm.symbolCheckBox(
            value=False,
            width=24,
            onImage='sanity_dbg_on.png',
            offImage='sanity_dbg_off.png',
            onCommand=pm.Callback(self.on_debug_button_click, True),
            offCommand=pm.Callback(self.on_debug_button_click, False)
            )

    def initialize_config_buttons(self):
        verbosity = 1
        if 'sanityVerbosity' in pm.env.optionVars:
            verbosity = su.clamp(pm.optionVar['sanityVerbosity'], 1, 3)
        index = verbosity - 1
        buttons = self.config_buttons['verbose'].getCollectionItemArray()
        self.config_buttons['verbose'].setSelect(buttons[index])
        dbg = False
        if 'sanityDebug' in pm.env.optionVars:
            dbg = pm.optionVar['sanityDebug']
            dbg = (False, True)[dbg]
        self.config_buttons['debug'].setValue(dbg)

    def set_families_to_current_config_state(self):
        if not 'sanityVerbosity' in pm.env.optionVars:
            pm.optionVar['sanityVerbosity'] = 1
        verbosity = pm.optionVar['sanityVerbosity']
        if not 'sanityDebug' in pm.env.optionVars:
            pm.optionVar['sanityDebug'] = False
        dbg = pm.optionVar['sanityDebug']
        dbg = (False, True)[dbg]
        self.set_familiy_verbosity(verbosity)
        self.set_familiy_debug(dbg)

    def on_verbosity_button_click(self, value):
        pm.optionVar['sanityVerbosity'] = value
        self.set_familiy_verbosity(value)

    def on_debug_button_click(self, value):
        pm.optionVar['sanityDebug'] = value
        self.set_familiy_debug(value)

    def set_familiy_verbosity(self, value):
        for family in self.families:
            for check_ui in family.get_check_uis():
                check_ui.set_verbosity(value)
            family.get_aggregator_ui().set_verbosity(value)

    def set_familiy_debug(self, value):
        for family in self.families:
            for check_ui in family.get_check_uis():
                check_ui.sanity_check.set_debug(value)
            family.get_aggregator_ui().sanity_check.set_debug(value)
        self.DEBUG = value

    def create_buttons(self):
        pm.setParent(self.form)
        b1 = pm.button(
            label='Add Family',
            command=pm.Callback(self.browse_for_family)
            )
        b2 = pm.button(
            label='Remove Family',
            command=pm.Callback(self.delete_current_family)
            )
        b3 = pm.button(
            label='Close',
            command=('sanity.pm.deleteUI(\"' + self + '\", window=True)')
            )
        pm.formLayout(
            self.form,
            edit=True,
            attachControl=[(self.tabs, 'bottom', 2, b1), (self.tabs, 'top', 2, self.config_row)],
            attachPosition=[(b1, 'right', 2, 33), (b2, 'left', 2, 33),
                     (b2, 'right', 2, 66), (b3, 'left', 2, 66)],
            attachNone=[(b3, 'top'), (b2, 'top'), (b1, 'top')],
            attachForm=[
                (b1, 'left', 2),
                (b1, 'bottom', 2),
                (b2, 'bottom', 2),
                (b3, 'bottom', 2),
                (b3, 'right', 2),
                (self.tabs, 'right', 2),
                (self.tabs, 'left', 2),
                (self.config_row, 'top', 2),
                (self.config_row, 'right', 2),
                (self.config_row, 'left', 2)]
            )


    # based on the current tab index, we want to
    # get the family package name and save its value in
    # the sanityCurrentTab option var
    def on_tab_changed(self):
        tab_id = self.tabs.getSelectTabIndex()
        if tab_id > 0:
            pm.optionVar['sanityCurrentTab'] = self.families[(
                tab_id - 1)].package_name

    # set the optionVar to the list
    # of family package names
    def sync_sanity_packages_option_var(self):
        pm.optionVar.pop('sanityPackages')
        if self.families:
            names = []
            for f in self.families:
                names.append(f.package_name)
            pm.optionVar['sanityPackages'] = names

    # on opening the window, add a tab for each
    # family stored in the option var. If no
    # saved families, open the file dialog
    def populate(self):
        pm.setParent(self.tabs)
        if 'sanityPackages' in pm.env.optionVars:
        #     self.browse_for_family()
        # else:
            current_package = None
            if 'sanityCurrentTab' in pm.env.optionVars:
                current_package = pm.optionVar['sanityCurrentTab']

            ov = pm.optionVar['sanityPackages']
            if not isinstance(ov, pm.language.OptionVarList):
                ov = [ov]

            select_tab_id = 0
            curr = 1
            for package_name in ov:
                if self.register_family(package_name):
                    if package_name == current_package:
                        select_tab_id = curr
                    curr += 1
            if select_tab_id > 0:
                self.tabs.setSelectTabIndex(select_tab_id)
            self.sync_sanity_packages_option_var()
            self.set_families_to_current_config_state()

    # remove a family and its UI, then sync optionVar
    def delete_current_family(self):
        tab_id = self.tabs.getSelectTabIndex()
        if tab_id == 0:
            pm.displayWarning('No tabs to delete')
        else:
            index = tab_id - 1
            num_c = self.tabs.getNumberOfChildren()
            num_f = len(self.families)
            if not num_f == num_c:
                pm.displayError('num_f num_c dont match')
                return
            pm.deleteUI(self.families.pop(index).column)
            self.sync_sanity_packages_option_var()

    # browse for a package containing a family of sanity checks.
    # try to register
    def browse_for_family(self):
        d = os.path.join(os.path.dirname(os.path.dirname(
            os.path.abspath(__file__))), 'family')

        #d = pm.workspace.getPath()
        cap = 'Import a sanity python package'
        ok = 'Import'
        entries = pm.fileDialog2(caption=cap, okCaption=ok, fileFilter='*.py',
                                 dialogStyle=2, fileMode=3, dir=d)
        if not entries:
            pm.displayWarning('Nothing Selected')
            return
        if self.register_family(os.path.basename(entries[0])):
            self.sync_sanity_packages_option_var()
            self.set_families_to_current_config_state()

    def package_registered(self, package_name):
        for f in self.families:
            if f.package_name == package_name:
                return True
        return False

    def register_family(self, package_name):

        if not self.package_registered(package_name):
            if not self.DEBUG:
                try:
                    family = sfm.Family(package_name, self.tabs)
                    self.families.append(family)
                    return True
                except ImportError:
                    pm.displayWarning(package_name + ' is not a valid package')
                    return False
            else:
                family = sfm.Family(package_name, self.tabs)
                self.families.append(family)
                return True
        else:
            pm.displayWarning(package_name + ' is already registered')
