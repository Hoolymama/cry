import inspect
import pymel.core as pm

"""
Module contains class to build UI around a SanityCheck

.. moduleauthor:: Julian Mann <julian.mann@gmail.com>

"""
class SanityCheckUI(object):

    def __init__(self, sanity_check, **kwargs):
        """Build the UI for a standard SanityCheck.

        :param sanity_check: The SanityCheck object.
        :type sanity_check: SanityCheck.
        """
        self.sanity_check = sanity_check
        self._config = {}
        self._config['verbosity'] = 1
        self.original_objects = []
        self.sanity_check.register_progress_functions(
            self.open_progress_window,
            self.update_progress_window,
            self.close_progress_window
            )
        self._init_ui()

    def set_verbosity(self, val):
        """Set verbosity.

        :param val: Verbosity level 1, 2, or 3.
        :type val: int.
        """
        if val < 1:
            self._config['verbosity'] = 1
        elif val > 3:
            self._config['verbosity'] = 3
        else:
            self._config['verbosity'] = val

    # checked state option var getter
    def get_stored_check_state(self):
        state = False
        varname = self.sanity_check.get_camel_name()
        if varname in pm.env.optionVars:
            state = pm.optionVar[varname]
        return state

    # checked state option var setter
    def set_stored_check_state(self):
        varname = self.sanity_check.get_camel_name()
        pm.optionVar[varname] = self.activeCB.getValue()

    # progress
    def progress_status(self, value):
        status = self.sanity_check.get_progress_action() + '...: '
        status += (
            str(value) + '/' +
            str(self.sanity_check.get_progress_todo())
            )
        return status

    def open_progress_window(self):
        label = self.sanity_check.label()
        action = self.sanity_check.get_progress_action()
        pm.progressWindow(title=(action + ' ' + label),
                          progress=0,
                          max=self.sanity_check.get_progress_todo(),
                          status=self.progress_status(0),
                          isInterruptable=True)

    def update_progress_window(self, amount):
        if pm.progressWindow(query=True, isCancelled=True):
            pm.progressWindow(endProgress=True)
            raise KeyboardInterrupt
        status = self.progress_status(amount)
        pm.progressWindow(edit=True, progress=amount, status=status)

    def close_progress_window(self):
        pm.progressWindow(endProgress=True)

    # copy the selection list before calling chek or fix
    # because a plugin author may modify the list
    # and that could break things when running all checks.
    def on_check_button_click(self, objects=None):
        self.hilight_list_button.setEnable(False)
        self.set_status_light()
        self.fix_button.setEnable(False)
        self.reset_button.setEnable(False)
        if not objects:
            objects = pm.ls(selection=True)
        self.remember_original_objects(objects)
        safe_objects = list(objects)
        will_fix = self.sanity_check.run_check(safe_objects)
        self.fix_button.setEnable(will_fix)
        self.select_and_enable_hilight_list()
        self.reset_button.setEnable(True)
        self.set_status_light('auto')
        self.display_output()

    def on_fix_button_click(self, objects=None):
        if not objects:
            objects = pm.ls(selection=True)
        safe_objects = list(objects)
        self.sanity_check.run_fix(safe_objects)
        self.fix_button.setEnable(False)
        self.select_and_enable_hilight_list()
        self.set_status_light('auto')
        self.display_output()

    def on_reset_button_click(self):
        self.sanity_check.before_reset_button_click()
        self.sanity_check.clear_log()
        self.sanity_check.clear_hilight_list()
        self.sanity_check.set_state(None)
        self.select_and_enable_hilight_list()
        self.set_status_light()
        self.fix_button.setEnable(False)
        self.reset_button.setEnable(False)
        self.select_original_objects()
        self.clear_original_objects_list()
        self.sanity_check.after_reset_button_click()

    def on_status_button_click(self):
        self.display_output()

    # selects everything in the objects stored selection list.
    def on_hilight_list_button_click(self):
        hl = self.sanity_check.get_hilight_list()
        if hl:
            pm.select(hl)
        self.sanity_check.after_select_hilight_list()

    def clear_original_objects_list(self):
        self.original_objects = []

    def remember_original_objects(self, objects):
        self.original_objects = objects

    def get_original_objects(self):
        if self.original_objects:
            return self.original_objects
        return False

    def select_original_objects(self):
        pm.select(clear=True)
        if self.original_objects:
            try:
                sel = pm.ls(self.original_objects)
                pm.select(sel)
            except:
                pm.displayError('Could not restore selection. Maybe some nodes were deleted.')

    ##################################
    ############ PRIVATE #############
    ##################################

    def select_and_enable_hilight_list(self):
        hl = self.sanity_check.get_hilight_list()
        if hl:
            pm.select(hl)
            self.sanity_check.after_select_hilight_list()
            self.hilight_list_button.setEnable(True)
        else:
            self.hilight_list_button.setEnable(False)

    def _set_fix_button(self, will_fix):
        self.fix_button.setEnable(will_fix)

    # show info, warnings, errors - (currently in the script editor)
    def display_output(self):
        v = self._config['verbosity']
        log = self.sanity_check.get_log(verbose=v)
        if log:
            for l in log:
                if l[1] == 'i':
                    pm.system.displayInfo(l[0])
                elif l[1] == 'w':
                    pm.system.displayWarning(l[0])
                else:
                    pm.system.displayError(l[0])
            print('')

    # set pass/fail/off ui indicators
    def set_status_light(self, state='off'):
        if state == 'auto':
            state = self.sanity_check.get_state()
        if state == 'failed':
            self.status_light.setImage('red_light.png')
        elif state == 'passed':
            self.status_light.setImage('green_light.png')
        else:
            self.status_light.setImage('off_light.png')

    def _init_ui(self):
        self.frame = pm.frameLayout(labelVisible=False)

        self.row = pm.rowLayout(
            numberOfColumns=7,
            columnWidth=[(1, 25), (2, 90), (3, 65), (4, 65), (5, 25), (6, 25), (7, 65)],
            adjustableColumn=2,
            columnAlign=(1, 'left'),
            columnAttach=[(1, 'both', 0), (2, 'both', 0), (
                3, 'both', 0), (4, 'both', 0), (5, 'both', 0), (6, 'both', 0), (7, 'both', 0)]
        )

        check_state = self.get_stored_check_state()
        self.activeCB = pm.checkBox(
            label='',
            value=check_state,
            annotation='Include this check when running ALL checks',
            changeCommand=pm.Callback(self.set_stored_check_state)
            )
        self.text = pm.text(
            label=self.sanity_check.label(),
            annotation=self.sanity_check.tips['label'].split("\n")[0]
            )
        self.check_button = pm.button(
            label='Check',
            annotation=self.sanity_check.tips['check'].split("\n")[0],
            command=pm.Callback(self.on_check_button_click)
        )

        self.fix_button = pm.button(
            label='Fix',
            annotation=self.sanity_check.tips['fix'].split("\n")[0],
            enable=False,
            visible=self.sanity_check.is_fixable(),
            command=pm.Callback(self.on_fix_button_click)
        )
        self.hilight_list_button = pm.symbolButton(
            image='red_pick.png',
            enable=False,
            annotation=self.sanity_check.tips['hilight'].split("\n")[0],
            command=pm.Callback(self.on_hilight_list_button_click)
        )
        self.status_light = pm.symbolButton(
            image='off_light.png',
            annotation='Click to display output',
            command=pm.Callback(self.on_status_button_click)
        )
        self.reset_button = pm.button(
            label='Reset',
            annotation=self.sanity_check.tips['reset'].split("\n")[0],
            enable=False,
            command=pm.Callback(self.on_reset_button_click)
        )


class SanityCheckAllUI(SanityCheckUI):
    """
    Special class to build a UI around an Aggregator SanityCheck
    """
    def __init__(self, sanity_check, **kwargs):
        super(SanityCheckAllUI, self).__init__(sanity_check, **kwargs)
        self.activeCB.changeCommand(pm.Callback(self.set_family_checkboxes))
        self.text.setFont('boldLabelFont')
        self.row.setBackgroundColor((0.3, 0.3, 0.4))
        self.row.setEnableBackground(True)
        # self.fix_button.setVisible(False)

    def set_family_checkboxes(self):
        state = self.activeCB.getValue()
        checkuis = self.sanity_check.get_check_uis()
        for c in checkuis:
            c.activeCB.setValue(state)
            c.set_stored_check_state()
