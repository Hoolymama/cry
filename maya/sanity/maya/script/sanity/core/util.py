import pymel.core as pm
from collections import defaultdict


def group_components_by_object(objects):
    result = defaultdict(list)
    for o in objects:
        parts = o.split('.')
        if len(parts) > 1:
            result[parts[0]].append(o)
    return result


def get_meshes(objects):
    result = pm.ls(objects, dag=True, leaf=True, v=True, ni=True, type='mesh')
    for k in group_components_by_object(objects).keys():
        result.append(pm.PyNode(k))
    return result


def get_geometry(objects):
    result = pm.ls(
        objects, dag=True, leaf=True, v=True, ni=True, type=(
            'mesh', 'nurbsSurface', 'subdiv'))
    return result


def get_transforms(objects):
    result = pm.ls(objects,  transforms=True)
    return result


def is_constraint(obj):
    try:
        pm.nt.Constraint(obj)
        return True
    except:
        return False

def clamp(n, minn, maxn):
    return max(min(maxn,n),minn)

KEYABLE_ATTS = ['rotateX', 'rotateY', 'rotateZ', 'scaleX', 'scaleY',
                    'scaleZ', 'translateX', 'translateY', 'translateZ', 'visibility']
