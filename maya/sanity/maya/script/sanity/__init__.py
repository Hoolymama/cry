import pymel.core as pm
import maya.mel as mm
from sanity.core import window as sw

# import sanity.family
# for family in ['sanity.family.polygeo', 'sanity.family.modelpub']:
#     f = __import__(family, fromlist=['*'])
#     for mod in getattr(f,'__all__'):
#         submod = family+ '.' + mod
#         __import__(submod, fromlist=['*'])


def setup(moduleName):
    moduleMenu = mm.eval("module.menuName \"" + moduleName + "\";")

    pm.setParent(moduleMenu, menu=True)
    # make some menu items under the sanity menu
    # m = pm.uitypes.CommandMenuItem()
    # m.setCommand('sanity.ui()')
    # m.setLabel('Sanity Checker')

    pm.menuItem(label='Sanity Checker', command=pm.Callback(sw.unique_sanity_window))
    #pm.menuItem(label='Sanity Checker Debug', command=pm.Callback(sw.SanityWindow, 'debug'))
     # pm.menuItem(label='Create another menu item', command = 'command')
    # mm.eval("module.subMenu \""+moduleMenu+"\" \"Foo|Bar\";")
    # pm.menuItem(label='Create a deep menu item', command = 'command')
    # pm.menuItem(label='Create another deep menu item', command = 'command')

    pm.setParent(moduleMenu, menu=True)
